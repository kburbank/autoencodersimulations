.. Autoencoder documentation master file, created by
   sphinx-quickstart on Wed Jun 18 16:57:58 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Autoencoder's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 3

.. automodule:: autoencoder.weights
   :members:

.. automodule:: autoencoder.plasticity
   :members:

.. automodule:: autoencoder.autoEncoder
   :members:

.. automodule:: autoencoder.network
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

