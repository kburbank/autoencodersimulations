from distutils.core import setup

requires = [
    'sumatra',
    'numpy',
    'numexpr',
    'cython',
    'tables',
    'scipy',
    'prettyplotlib',
    'wand'
    ]


setup(
    name='autoencoder',
    version='2.3',
    packages=['autoencoder','autoencoder.defaultmodels','models'],
    install_requires=requires,
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    package_data={'autoencoder': ['version_history.txt']},
    long_description=open('README.txt').read(),
)
