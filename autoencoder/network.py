from __future__ import division
from pylab import figure, clf, subplot, cla, hist, gcf
import numpy as np

from . import save
from . import datasets
import cPickle as pickle
from copy import copy, deepcopy

from .util import Printer





class Network(object):

	""" A container class to collect weights, plasticity, and presentation objects.

	A simulation will generally have weights, plasticity rules, and presentation objects. This class collects all of them and handles saving  them into
	a file as well as making plots during a simulation.

	Args:
			config: a SafeConfigParser object
			nextdata: an example input stimulus, used to determine the number of neurons in the lower layer

	"""

	def __init__(self,config,ModelClass, architecture,**kwargs):
		self.config = config
		self.config.model = ModelClass
		self.architecture = architecture
		self.ModelClass = ModelClass
		self.add_data()
		self.P=self.ModelClass.presentation(self.config,architecture = self.architecture,refresh=True)
		self.plasticity=self.ModelClass.plasticity(self.config, architecture = self.architecture,refresh = True)
		self.config.presentation_type=self.ModelClass.presentation
		self.weights=ModelClass.weights(self.config, architecture = self.architecture)

		if self.config.configObject.get('simulation','do_saves'):
			self.s = save.Save(self.config,self.P,self.weights,architecture = self.architecture)
			self.filename = self.s.filename

	def add_data(self):
		datasettype = self.config.datatype
		assert hasattr(
			datasets, datasettype), "I don't understand the dataset name %s. Make sure it's one that's defined in autoencoder.datasets" % datasettype
		self.data = getattr(datasets, datasettype)(self.config)



	def setWeightsFromSaved(self, N=[], pN=[]):
		wl = self.s.weights(N, pN,ww=self.weights,i=0)
		#       assert isinstance(wl, weights.Weights)
		self.weights = wl


 
	def doPresentation(self, updateWeights=True, set_type='train'):
		self.P.doPresentation(
			self.data.nextdata(set_type=set_type), self.weights)
		if updateWeights:
			self.plasticity.updateAll(self.weights, self.P)
		return self.P


	


	def addSavedPresentations(self, N, set_type='test'):
		for i in xrange(0, N):
			self.doPresentation(updateWeights=False, set_type=set_type)
			self.s.append(self.P)

    


	def addSavedPresentationsAllWeights(self, num):
		currWeights = self.weights
		wl = self.s.weightsList()
		for N in wl:
			Printer('Adding presentations for pN = %d' % N)
			self.data.n_test = 0
			self.setWeightsFromSaved(pN=N)
			self.addSavedPresentations(num)
		self.weights = currWeights


	def __repr__(self):
		part1 = "<Network class\r\
		Plasticity type: %s \r\
		Presentation type: %s \r\
		Current weight update number: %s \r\
		" % (self.plasticity.__class__.__name__, self.P.__class__.__name__, self.weights.number_of_weight_updates)
		return part1


	def __getstate__(self):
		d = dict(self.__dict__)
		d['data'] = self.data.__class__.__name__
		return d

	def __setstate__(self,d):
		self.__dict__ = d
		self.data =  getattr(datasets, d['data'])(self.config)

	def dump(self, filename):
		pickle.dump(self, open(filename, "wb"),-1)


