from __future__ import division
import numpy as np
from pylab import cla, scatter, hist
from numpy import sum, mean, clip, ones, dot, corrcoef, zeros
from math import *
import tables
from autoencoder.aeconfig import configView


class Presentation(object):

	architecture = None
	calculated_variables_created = False


	def __init__(self, config,architecture = None, refresh = False):
		if architecture is not None:
			self.__class__.architecture = architecture
		assert self.architecture is not None
		if refresh:
			self.__class__.calculated_variables_created = False
		self.config = config
		self._applyConfigVariables(config)
		self.applyCalculatedVariables()
		self.setupFields()
		self.cv=configView(self.config.configObject,sectionname='presentation')
		self.cvi=configView(self.config.configObject,sectionname='presentation',function_name='getint')

	def __getstate__(self):
		d = self.__dict__
		d['architecture']=self.__class__.architecture
		return d

	def __setstate__(self,d):
		self.__dict__ = d
		self.__class__.architecture = d['architecture']
		self.__class__.calculated_variables_created = False
		del self.architecture
		self._applyConfigVariables(self.config)
		self.applyCalculatedVariables()

	def setupFields(self):
		raise NotImplementedError()

	def _applyConfigVariables(self,config):
		pass

	def applyCalculatedVariables(self):
		pass

	def rasterPlot(self):
		pass

	def get_reconstruction(self,input_rates,weights):
		# for integrate and fire, we actually need to run the simulation with and without feedback
		# but for numerical work, x2 will suffice.
		return self.x2


	def reconstructionCorr(self,weights,**kwarg):
		reconstruction=self.get_reconstruction(self.input_rates,weights,**kwarg)
		if (any(self.input_rates>0) and any(reconstruction>0)):
			theCorr = corrcoef(1.0*self.input_rates, 1.0*reconstruction)[0, 1]
			if isnan(theCorr): 
				return 0
			else:
				return theCorr
		else:
			return 0

	def squaredError(self):
		theDiff = self.x2 - self.input_rates
		return dot(theDiff, theDiff)

