from ConfigParser import SafeConfigParser
import os
from autoencoder import datasets
import autoencoder
from pprint import pprint
#from sumatra.parameters import SafeConfigParser
from matplotlib import rc
import StringIO
import pylab
import operator
from configobj import ConfigObj
from ConfigParser import SafeConfigParser

paramstring = '''

# From analysis.py
analysis_output_dir = string()

# From datasets.py
datadir = string()

# From network.py
datatype = string(default = "MNIST_Dataset")

# From simulation.py
do_saves = boolean(default = True)
max_presentations = integer(min = 1, default = 50000)
number_test_images = integer(min = 0, default = 100)
background_mode = boolean(default = True)
sumatradir = string()




'''

class configView(object):
	
	def __init__(self,configObject,sectionname,function_name='getfloat'):
		self.configObject = configObject
		self.sectionname=sectionname
		self.function_name=function_name
		self.function = getattr(self.configObject,function_name)
		
	def __getattr__(self,name):
			if name in self.__dict__:
				return self.__dict__[name]
			elif 'function' in self.__dict__:
				return self.__dict__['function'](self.sectionname,name)
			else :
				raise AttributeError()


	def __getstate__(self):
		d = {'configObject':self.configObject,'sectionname':self.sectionname,'function_name':self.function_name}
		return d

	def __setstate__(self,d):
		self.configObject = d['configObject']
		self.sectionname = d['sectionname']
		self.function_name = d['function_name']
		self.function =  getattr(self.configObject,self.function_name)

class Config(object):

	def __init__(self, configfile,configString='',paramstring='', **kwargs):

		config = self.getConfig(configFileName=configfile,configString=configString,paramstring=paramstring)
		self.configObject = config
		self.paramstring = paramstring
		for key, value in kwargs.iteritems():
			notfoundyet = True
			for section in self.configObject.sections():
				if notfoundyet:
					if self.configObject.has_option(section,key):
						self.configObject.set(section,key,'%s'%value)
						notfoundyet = False
						print('Set key "%s" in section "%s" to value "%s"'%(key,section,value))
			if notfoundyet:
				print('Uh-oh, could not find a section for key %s'%key)
		self.setValuesFromConfig()
		self.n_x = getattr(datasets, self.datatype).n_x
		self.combine = getattr(datasets, self.datatype).combine

	def getConfig(self, configFileName='',configString='',paramstring=''):
		program_path = os.path.dirname(__file__)
#		print program_path
		home = os.path.expanduser("~")
		computer_config_path = '%s/.autoencoder.INI' % home
		# build list of config files. config parser is smart, if it doesn't
		# find a file it simply doesn't read it.
		
		config=SafeConfigParser()
		mylist=[]
		config.readfp(StringIO.StringIO(paramstring))
		mylist.append(config.read(computer_config_path))
		mylist.append(config.read(configFileName))
		config.readfp(StringIO.StringIO(configString))
#		print mylist
#		print configString
		return config


	def setValuesFromConfig(self):
		config = self.configObject

	
	
		self.analysis_output_dir = config.get('simulation', 'analysis_output_dir')
		self.write_over_output_files = config.getint('simulation','write_over_output_files')
		#if self.savedir == '':
		#	self.savedir = '%s/Outputs' % os.path.dirname(__file__)
		#if self.savedir == '':
		#	self.savedir = '%s/Data' % os.path.dirname(__file__)
		self.sumatradir = config.get('simulation', 'sumatradir')
	
		self.experiment_name = config.get('simulation','experiment_name')
		self.len_savelist = 200
		self.number_test_images=config.getint('simulation','number_test_images')

		self.datatype = config.get('simulation', 'datatype')
		self.runtype = config.get('simulation', 'runtype')

	

	def __repr__(self):
		pprint(self.__dict__)

	def writeConfig(self, configFileName):
		with open(configFileName, 'wb') as configfile:
			self.configObject.write(configfile)

	def plotConfig(self,modelname,default_config=None):
		rc('text', usetex=False)
		current_config = self.configObject
		if not default_config:
			default_config = Config(configfile='',paramstring = self.paramstring).configObject
		thetext = 'AE version %s, %s\n'%(autoencoder.__version__,modelname)
		for section in current_config.sections():
			added_section_name = False
			d = DictDiffer(current_config._sections[section],default_config._sections[section])
			if d.changed():
				if added_section_name == False:
					thetext += '[%s]\n'%section
					added_section_name = True

					ch = sorted(d.changed())
		#ch=sorted(ch.iteritems(), key=operator.itemgetter(0))

					val='experiment_name'
					if val in ch:
						ch.insert(0, ch.pop(ch.index(val)))  # put experiment_name at the top of the list for easy reading


					if 'analysis_output_dir' in ch: del ch[ch.index('analysis_output_dir')]
					# don't display the output path name, since it's long and we don't care
		
					for j in ch:
						if j != 'configObject':
							thetext += j + ':  ' + str(current_config.get(section,j)) + '\n'
		ax = pylab.gca()
		pylab.text(0.5, 0.5, thetext, horizontalalignment='center',
				   verticalalignment='center', transform=ax.transAxes, fontsize=12)
		pylab.gca().get_xaxis().set_ticks([])
		pylab.gca().get_yaxis().set_ticks([])


class DictDiffer(object):

	"""
	Calculate the difference between two dictionaries as:
	(1) items added
	(2) items removed
	(3) keys same in both but changed values
	(4) keys same in both and unchanged values
	"""

	def __init__(self, current_dict, past_dict):
		self.current_dict, self.past_dict = current_dict, past_dict
		self.set_current, self.set_past = set(
			current_dict.keys()), set(past_dict.keys())
		self.intersect = self.set_current.intersection(self.set_past)

	def added(self):
		return self.set_current - self.intersect

	def removed(self):
		return self.set_past - self.intersect

	def changed(self):
		return set(
			o for o in self.intersect if self.past_dict[o] != self.current_dict[o])

	def unchanged(self):
		return set(
			o for o in self.intersect if self.past_dict[o] == self.current_dict[o])
