from IPython import parallel
#import sys
#sys.path.append('/Users/kendra/Dropbox/WorkDocuments/autoencoderSimulations')
import time
from ConfigParser import SafeConfigParser, RawConfigParser
import tempfile
import imp
from IPython.parallel.util import interactive
import autoencoder
import os



class Batch(object):

	def __init__(self,configFileName):
		import models

		#self.c = parallel.Client()
		#self.view = self.c.load_balanced_view()
		self.configFileName=configFileName

		#temp_model = __import__('models.%s'%modelfilename)
		#model_module = getattr(temp_model,modelfilename)

		
		#self.tempfiles=self.generate_temp_config_files(self.configFileName)
		self.experiment_strings, self.modelname,self.specific_model = self.read_bulk_ini_py(self.configFileName)
		self.model = getattr(getattr(models,self.modelname),self.specific_model)


	def read_bulk_ini_py(self,configFileName):
		configstrings = imp.load_source('configstrings',configFileName)
		filenamebase = os.path.splitext(os.path.basename(configFileName))[0]
		configstrings.shared += '''
[simulation]
analysis_output_dir=%s'''%filenamebase
		out_experiments = []
		for thisexperiment in configstrings.experiments:
			out_experiments.append(configstrings.shared + thisexperiment)
		modelname = configstrings.model
		if hasattr(configstrings,'specific_model'):
			specific_model=configstrings.specific_model
		else:
			specific_model='Model'
		
		return out_experiments, modelname, specific_model





	def run(self, experimentnumber):


		self.sim = autoencoder.simulation.Simulation(model=self.model,runNow=True,configString=self.experiment_strings[experimentnumber])
	

