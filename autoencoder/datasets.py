import cPickle
import gzip
import numpy
from os.path import expanduser
from numpy import shape,mean
import scipy.io
import random
import pylab
import matplotlib
import os




class Dataset(object):

	n_x=[]
	combine = []
	data_loaded = False

	def __init__(self,config):
		self.config = config
		self.initialize_data_counters()
		if self.data_loaded == False:
			self.load_data()
			self.data_loaded = True
		

	def load_data(self):
		raise NotImplementedError()

	def nextdata(self, increment=True, **kwargs):
		raise NotImplementedError()


	def initialize_data_counters(self):
		pass


class filtered_Images_Dataset(Dataset):
	# the one with the images from olshausen and field

	n_x = 512
	combine = True


	def load_data(self):
		#datafile = 'IMAGES.mat'

		if os.environ.has_key('AE_DATA_DIR'):
			data_path = os.environ['AE_DATA_DIR']
		else:
			import inspect
			#package_directory = os.path.dirname(os.path.abspath(inspect.getsourcefile(autoencoder)))
			package_directory = os.path.dirname(__file__)
			data_path = os.path.join(package_directory, '../../data/inputs')
#			data_path = self.__file__+'../data/'
			print 'Data import path is %s' % data_path

		datafile = '%s/IMAGES.mat' % data_path


		if (not os.path.isfile(datafile)):
			import urllib
			origin = (
				'http://redwood.berkeley.edu/bruno/sparsenet/IMAGES.mat'
			)
			print 'Downloading data from %s' % origin
			urllib.urlretrieve(origin, datafile)



		mat = scipy.io.loadmat(datafile)
		self.train_set_x = mat['IMAGES']
		self.train_set_x /= numpy.amax(self.train_set_x)
		self.n_train = 1
		self.number_training_samples = 1000
	

	def nextdata(self, increment=True, **kwargs):
		imnum = random.randint(1, 9)
		x = random.randint(7, 512 - 22)
		y = random.randint(7, 512 - 22)
		patch = self.train_set_x[x:x + 16, y:y + 16, imnum].reshape(-1)
		return numpy.concatenate(
			(patch.clip(0, 100), (-patch).clip(0, 100)), axis=0)

	def showImages(self):
		pylab.clf()
		for i in xrange(0, 8):
			pylab.subplot(3, 3, i + 1)
			pylab.imshow(self.train_set_x[:, :, i], cmap='gray')

class filtered_Images_Only_High_Dataset(filtered_Images_Dataset):

	def nextdata(self,**kwargs):
		nd=0
		tries = 0
		while mean(nd)<0.06 and tries < 1000:
			tries +=1
			nd = super(filtered_Images_Only_High_Dataset, self).nextdata(kwargs)
		return nd

class filtered_Images_High_Normalized_Dataset(filtered_Images_Only_High_Dataset):

	def nextdata(self,**kwargs):
		nd=super(filtered_Images_High_Normalized_Dataset,self).nextdata(**kwargs)
		nd/=numpy.mean(nd)
		nd*=.1
		return nd

class filtered_Images_No_Center_Dataset(filtered_Images_Dataset):
	# the one with the images from olshausen and field

	n_x = 512 / 2
	combine = False

	def nextdata(self, increment=True, **kwargs):
		imnum = random.randint(1, 9)
		x = random.randint(7, 512 - 22)
		y = random.randint(7, 512 - 22)
		patch = self.train_set_x[x:x + 16, y:y + 16, imnum].reshape(-1)
		patch /= numpy.std(patch)
		return patch



class MNIST_Dataset(Dataset):

	n_x = 14*14*2
	combine = True

	def load_data(self):
		# Load the dataset

		if os.environ.has_key('AE_DATA_DIR'):
			data_path = os.environ['AE_DATA_DIR']
		else:
			import inspect
			#package_directory = os.path.dirname(os.path.abspath(inspect.getsourcefile(autoencoder)))
			package_directory = os.path.dirname(__file__)
			data_path = os.path.join(package_directory, '../../data/inputs')
#			data_path = self.__file__+'../data/'
			print 'Data import path is %s' % data_path

		dataset = '%s/mnist.pkl.gz' % data_path

		#dataset = 'mnist.pkl.gz'

		if (not os.path.isfile(dataset)):
			import urllib
			origin = (
				'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'
			)
			print 'Downloading data from %s' % origin
			urllib.urlretrieve(origin, dataset)

		print '... loading data'
		#datafile = 'mnist.pkl.gz'
		f = gzip.open((dataset), 'rb')
		train_set, valid_set, test_set = cPickle.load(f)
		f.close()
		self.test_set_x, self.test_set_y = self.unpack(test_set)
		self.valid_set_x, self.valid_set_y = self.unpack(valid_set)
		self.train_set_x, self.train_set_y = self.unpack(train_set)
		self.number_training_samples = numpy.shape(self.train_set_x)[0]
		


	def initialize_data_counters(self):
		self.n_test = 0
		self.n_valid = 0
		self.n_train = 0
 

	def preprocess(self, a):
		a -= numpy.mean(a, axis=0)
		return a

	def rebin(self, a):
		a = a.reshape((-1, 28, 28))
		a = a[:, 0:-1:2, 0:-1:2]
		a = a.reshape((-1, 14 * 14))
		return a

	def unpack(self, the_set):
		set_x, set_y = the_set
		set_x = self.preprocess(set_x)
		set_x = self.rebin(set_x)
		if self.combine:
			set_x = numpy.concatenate(
				(set_x.clip(0, 100), (-set_x).clip(0, 100)  ), axis=1)

		return set_x, set_y

	def nextdata(self, set_type='train', increment=True):
		if self.n_train > self.number_training_samples - 2:
			self.n_train = 1
		if increment:
			setattr(self, 'n_' + set_type, getattr(self, 'n_' + set_type) + 1)
		the_num = getattr(self, 'n_' + set_type)
		the_set = getattr(self, set_type + '_set_x')

		return the_set[the_num, :]

class MNIST_Normalized_Dataset(MNIST_Dataset):

	def nextdata(self, set_type='train', increment=True):
		if self.n_train > self.number_training_samples - 2:
			self.n_train = 1
		if increment:
			setattr(self, 'n_' + set_type, getattr(self, 'n_' + set_type) + 1)
		the_num = getattr(self, 'n_' + set_type)
		the_set = getattr(self, set_type + '_set_x')

		return the_set[the_num, :]/numpy.mean(the_set[the_num,:])*0.075


class MNIST_Large_Dataset(MNIST_Dataset):
	n_x = 28*28*2

	def rebin(self, a):
		a = a.reshape((-1, 28* 28))
		return a


class MNIST_No_Center_Dataset(MNIST_Dataset):

	n_x = 196
	combine = False

	# def preprocess(self,a):
	# return a


class MNIST_No_Subtract_Dataset(MNIST_Dataset):

	n_x = 196
	combine = False

	def preprocess(self, a):
		return a


class Imagepatches_Dataset:

	def load_data(self):
		#        from tables import openFile
		# Load the dataset
		home = expanduser("~")
		self.train_set_x = numpy.load(('/tmp/data/image_patch_data.20.p.npy'))
  #      self.train_set_x= f.root.patches
		self.n_train = 0
 #       f.close()
		self.number_training_samples = numpy.shape(self.train_set_x)[0]

	def rebin(self, a, ndims, chop=0):
		siz = shape(a)
		if a.shape == ndims:
			return a.reshape(-1)

		else:
			# subsample separately for ON and OFF portions of the image. We
			# were getting possible artifacts from doing this wrong.
			atop = a[0:siz[0] / 2, :]
			abottom = a[siz[0] / 2:, :]
			sh = ndims[0] / 2, siz[0] // ndims[0], ndims[1], siz[1] // ndims[1]
			atop = atop.reshape(sh).mean(-1).mean(1)
			abottom = abottom.reshape(sh).mean(-1).mean(1)
			if chop == 0:
				a = numpy.concatenate((atop, abottom), axis=0)
			else:
				a = atop
#            imshow(-a.reshape((ndims,ndims)),cmap='Greys')
#            show()
			return a.reshape(-1)

	def nextdata(self, set_type='train', rebin_size=[], increment=True, **kwargs):
		if self.n_train == 39600:
			self.n_train = 1
		if set_type == 'train':
			n = self.n_train
			if increment:
				self.n_train += 1
			return self.rebin(self.train_set_x[n], rebin_size, **kwargs)
		else:
			print('Did not recognize data set name')
			return TypeError
