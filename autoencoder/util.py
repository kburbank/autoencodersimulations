import os
import sys
import errno

def add_unique_postfix(fn, extention):
    if not os.path.exists(fn + '.' + extention):
        return fn

    path, name = os.path.split(fn)
    name, ext = os.path.splitext(name)

    make_fn = lambda i: os.path.join(path, '%s(%d)%s' % (name, i, ext))

    for i in xrange(2, sys.maxsize):
        uni_fn = make_fn(i)
        if not os.path.exists(uni_fn + '.' + extention):
            return uni_fn

    return None


class Printer():

    """Print things to stdout on one line dynamically"""

    def __init__(self, data):
        sys.stdout.write("\r\x1b[K" + data.__str__())
        sys.stdout.flush()

def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise