from __future__ import division
from autoencoder import network, presentation, simulation, save
from pylab import figure, clf, subplot, cla, hist, gcf
import autoencoder
import numpy as np
from numpy import sum, mean, clip, ones, dot, corrcoef, zeros,shape,arange,reshape,sqrt

from math import *
import tables
from pylab import  cla,scatter, hist, imshow,gca,xlabel,ylabel,axis,title,tight_layout,locator_params,gca
from IPython.display import  clear_output
from copy import copy
import pylab
from tables import IntCol, FloatCol, Float32Col, UInt32Col
from matplotlib import rc
from mpl_toolkits.axes_grid1 import ImageGrid


class Plots(autoencoder.Plots):


	def plotWeightHistory(self,numberTimes=5,num_hid=10):
		net = self.net
		wl = net.s.weightsList()
		pnlist = np.unique(
			np.hstack((wl[0::int(len(wl) / numberTimes)], wl[-1])))
		n = len(pnlist)
		Wtest = net.weights.weightsColumn(num_hid)
		wid=shape(Wtest)[1]
		hi = shape(Wtest)[0]
		Wbig = np.zeros((hi,wid*n))

		for i, pn in enumerate(pnlist):
			net.setWeightsFromSaved(pN=pn)
			Wbig[:,i*wid:(i+1)*wid] = net.weights.weightsColumn(num_hid)
		#tr = weightsColumn(net.weights)
		#print(shape(tr))
		imshow(Wbig,cmap='gray')
		gca().set_xticks(arange(0,len(pnlist)*wid,wid+0.5)+8)

		gca().set_xticklabels(pnlist,rotation='vertical',horizontalalignment='center')
		xlabel(r'Number of stimulus presentations $\rightarrow$')
		ylabel('Hidden unit number')
		gca().set_yticks(arange(0,hi,wid)+8)
		gca().set_yticklabels(arange(0,num_hid)+1)


	def plotHistoricalReconstructionAttempts(self,pNs=[1,1000,100000],presnumber=0,factor=None):
		def removeticks(ax):
			ax.set_xticks(())
			ax.set_yticks(())
		fig=gcf()

		nrows=len(pNs)+1
		grid = ImageGrid(fig, 111, # similar to subplot(111)
					nrows_ncols = (nrows, 10), # creates 2x2 grid of axes
					axes_pad=0.05, # pad between axes in inch.
					label_mode  = "1")
		nx2 = self.architecture.n_x/2
		for i in range(0,10):
#			import ipdb; ipdb.set_trace()
			P=self.net.s.presentations(pN=pNs[-1],i=i+presnumber)
			toAdd=0
			for pn in pNs:
				self.net.setWeightsFromSaved(pN=pn)
				reconstruction=P.get_reconstruction(P.input_rates,self.net.weights,factor=factor)
				grid[toAdd+i].imshow(reshape(reconstruction[0:nx2]*1.0+reconstruction[nx2:]*-1.0,(sqrt(nx2),sqrt(nx2))),cmap='gray')
				removeticks(grid[toAdd+i])
				toAdd+=10
			grid[toAdd+i].imshow(reshape(P.input_rates.T[0:nx2]-P.input_rates.T[nx2:],(sqrt(nx2),sqrt(nx2))),cmap='gray')
			removeticks(grid[toAdd+i])

		extents = np.array([a.get_position().extents for a in grid])  #all axes extents
		bigextents = np.empty(4)   
		bigextents[:2] = extents[:,:2].min(axis=0)
		bigextents[2:] = extents[:,2:].max(axis=0)

		label_align = 'right'
		#text to mimic the x and y label. The text is positioned in the middle 
		labelpad=0.01  #distance between the external axis and the text
		#if numPresInstance:
		#	ylab_t = fig.text( bigextents[0]-labelpad, (bigextents[3]+bigextents[1])/2, 'Spiking reconstruction',
		#		rotation='horizontal', horizontalalignment = label_align, verticalalignment = 'center')
		#	ylab_t_bot = fig.text( bigextents[0]-labelpad, (bigextents[3]+bigextents[1])*.38, 'Numerical reconstruction',
		#		rotation='horizontal', horizontalalignment = label_align, verticalalignment = 'center')
		#else:

		ylab_t_bot = fig.text( bigextents[0]-labelpad, (bigextents[3]+bigextents[1])*.2, 'Input Stimulus',
		    rotation='horizontal', horizontalalignment = label_align, verticalalignment = 'center')

		ylab_t_bot = fig.text( bigextents[0]-labelpad, (bigextents[3]+bigextents[1])*.4, 'Final Reconstruction',
		    rotation='horizontal', horizontalalignment = label_align, verticalalignment = 'center')


		ylab_t_top = fig.text( bigextents[0]-labelpad, (bigextents[3]+bigextents[1])*.6, 'N=%d'%pNs[1],
		    rotation='horizontal', horizontalalignment = label_align, verticalalignment = 'center')

		ylab_t_top = fig.text( bigextents[0]-labelpad, (bigextents[3]+bigextents[1])*.8, 'N=%d'%pNs[0],
		    rotation='horizontal', horizontalalignment = label_align, verticalalignment = 'center')

		

	def plotReconstructionAttempts(self,pN=None,numPresInstance=None,presnumber=0,weights=None,factor=None):
		if not pN:
			pN=np.max(self.net.s.weightsList())
		if not weights:
			weights=self.net.s.weights(pN=pN,i=0)
#		rc('figure',figsize=(8.0, 4.2))
		def removeticks(ax):
			ax.set_xticks(())
			ax.set_yticks(())
		fig=gcf()
		numPresInstance=0
		if numPresInstance:
			nrows=3
		else:
			nrows=2
		grid = ImageGrid(fig, 111, # similar to subplot(111)
					nrows_ncols = (nrows, 10), # creates 2x2 grid of axes
					axes_pad=0.05, # pad between axes in inch.
					label_mode  = "1")

		for i in range(0,10):
#			import ipdb; ipdb.set_trace()
			P=self.net.s.presentations(pN=pN,i=i+presnumber)
			nx2 = self.architecture.n_x/2
			reconstruction=P.get_reconstruction(P.input_rates,weights,factor=factor)

			grid[i].imshow(reshape(P.input_rates.T[0:nx2]-P.input_rates.T[nx2:],(sqrt(nx2),sqrt(nx2))),cmap='gray')
			removeticks(grid[i])
			grid[10+i].imshow(reshape(reconstruction[0:nx2]*1.0+reconstruction[nx2:]*-1.0,(sqrt(nx2),sqrt(nx2))),cmap='gray')
			removeticks(grid[10+i])
			#if numPresInstance:
			#	numPresInstance.doPresentation(P.input_rates,weights)
			#	grid[20+i].imshow(reshape(numPresInstance.x2[0:nx2]-numPresInstance.x2[nx2:],(sqrt(nx2),sqrt(nx2))),cmap='gray')
			#	removeticks(grid[20+i])

		extents = np.array([a.get_position().extents for a in grid])  #all axes extents
		bigextents = np.empty(4)   
		bigextents[:2] = extents[:,:2].min(axis=0)
		bigextents[2:] = extents[:,2:].max(axis=0)

		label_align = 'right'
		#text to mimic the x and y label. The text is positioned in the middle 
		labelpad=0.01  #distance between the external axis and the text
		#if numPresInstance:
		#	ylab_t = fig.text( bigextents[0]-labelpad, (bigextents[3]+bigextents[1])/2, 'Spiking reconstruction',
		#		rotation='horizontal', horizontalalignment = label_align, verticalalignment = 'center')
		#	ylab_t_bot = fig.text( bigextents[0]-labelpad, (bigextents[3]+bigextents[1])*.38, 'Numerical reconstruction',
		#		rotation='horizontal', horizontalalignment = label_align, verticalalignment = 'center')
		#else:
		ylab_t_bot = fig.text( bigextents[0]-labelpad, (bigextents[3]+bigextents[1])*.45, 'Reconstruction',
			rotation='horizontal', horizontalalignment = label_align, verticalalignment = 'center')


		ylab_t_top = fig.text( bigextents[0]-labelpad, (bigextents[3]+bigextents[1])*.55, 'Input stimulus',
			rotation='horizontal', horizontalalignment = label_align, verticalalignment = 'center')
	
	def plotReconstructionReceptiveFields(self,pN=None,presNumber=0,Npres=5,factor=None):
		if not pN:
			pN=np.max(self.net.s.weightsList())
		
		xdim=sqrt(self.net.architecture.n_x/2)
		xhalf=self.net.architecture.n_x/2
		clf()
		for Nn in range(0,Npres):
			P=self.net.s.presentations(pN=pN,i=presNumber+Nn)
			x2=P.get_reconstruction(P.input_rates,self.net.weights,factor=factor)
			z1=P.z1*1.0
			sort_index = np.argsort(z1)
			N=10
			top_z1=sort_index[-N:]
		
			subplot(Npres,N+2,Nn*(N+2)+1)
			axis('off')
			if Nn==0:
				title('Input')
			imshow((P.input_rates[0:xhalf].reshape(xdim,xdim)-P.input_rates[xhalf:].reshape(xdim,xdim)),cmap='gray')
			for i in range(0,N):
			    zind = top_z1[N-i-1]

			    subplot(Npres,N+2,i+2+Nn*(N+2))
			    alphaval=(1.0*z1[zind])/(1.0*z1[top_z1[-1]])
			    imshow((self.net.weights.W[0:xhalf,zind].reshape(xdim,xdim)-self.net.weights.W[xhalf:,zind].reshape(xdim,xdim)),alpha=alphaval,cmap='gray')
			    axis('off')
			    #title('%d'%(i+1))
			#print top_z1[N-10:N]
			subplot(Npres,N+2,N+2+Nn*(N+2))
			#x2= np.dot(z1,self.net.weights.Wf.T).clip(0,100)

			#x2=P.get_reconstruction(P.input_rates,self.net.weights)
			imshow((x2[0:xhalf].reshape(xdim,xdim)*1.0+ x2[xhalf:].reshape(xdim,xdim)*-1.0),cmap='gray')
			axis('off')
			if Nn==0:
				title('Reconstruction')
		gcf().subplots_adjust(hspace=1, wspace=0)


	def makePlots(self):
		""" Plot some figures to monitor the progress of the ongoing simulation.  Plots current learned receptive fields,
		a raster plot from the most recent stimulus presentation, histograms the weights, the thetas (bias terms), and the changes
		to the weights during the most recent stimulus presentation.
		"""
		clear_output(wait=True)
		figure(1)
		clf()
		subplot(1, 2, 1)
		self.net.weights.showReceptiveFields()
		subplot(4, 2, 2)
		cla()
		hist(self.net.weights.avg_firing_rates.reshape(-1), 100)
		title('Average firing rates')
		locator_params(axis='x',nbins=4)
		locator_params(axis='y',nbins=2)
		gca().xaxis.get_major_formatter().set_powerlimits((0, 1))
		subplot(4, 2, 4)
		cla()
		self.net.weights.histTheta()
		locator_params(axis='x',nbins=4)
		locator_params(axis='y',nbins=2)
		gca().xaxis.get_major_formatter().set_powerlimits((0, 1))
		subplot(4, 2, 6)
		cla()
		self.net.weights.histW()
		locator_params(axis='x',nbins=4)
		locator_params(axis='y',nbins=2)
		gca().xaxis.get_major_formatter().set_powerlimits((0, 1))
		if any(self.net.weights.dW.reshape(-1)):
			subplot(4, 2, 8)
			hist(self.net.weights.dW.reshape(-1)
				 [abs(self.net.weights.dW.reshape(-1)) > 0], 100)
			title('Change in feedforward weights')
			locator_params(axis='x',nbins=4)
			locator_params(axis='y',nbins=2)
			gca().xaxis.get_major_formatter().set_powerlimits((0, 1))
		tight_layout()
		figure(2)
		self.net.P.rasterPlot()
		fig = gcf()




class Plasticity(autoencoder.Plasticity):
	# only works with its own type of weights...
	def calcDTheta(self, weights, P):
		return self.cv.thetaLearningRate * \
			(self.cv.pp - weights.avg_firing_rates)

	def updateAll(self, weights, P):
		dW, dWf = self._calcDW(weights, P)
		weights.avg_firing_rates += (1.0 / self.cv.firing_rate_timescale) * (P.frsum - weights.avg_firing_rates)
		dtheta = self.calcDTheta(weights, P)
		weights.changeWeights(dW=dW, dWf=dWf, dtheta=dtheta)
		return dW, dWf, dtheta

	def _calcDW(weights,P):
		raise NotImplementedError()

	def chunks(self, l, n):
		siz = np.shape(l)[1]
		if n < 1:
			n = 1
		return [[l[0][i:i + n], l[1][i:i + n]] for i in range(0, siz, n)]


class Presentation(presentation.Presentation):

	append_dict = {'input_rates':'input_rates','z1':'z1','x2':'x2','pN':'weight_update_number'}


	def setupFields(self):
		
		self.frsum = zeros(self.architecture.n_z)
		self.z1 = zeros(self.architecture.n_z)
		self.x2 = zeros(self.architecture.n_x)
		self.input_rates = zeros(self.architecture.n_x)
		self.presentation_number = 0
		self.weight_update_number = 0
		self.presentation_table = {
			"input_rates": tables.FloatCol(shape=(self.architecture.n_x)),
			"z1": tables.FloatCol(shape=self.architecture.n_z),
			"x2": tables.FloatCol(shape=self.architecture.n_x),
			"pN": tables.IntCol()
		} 

	def get_reconstruction(self,input_rates,weights):
		# for integrate and fire, we actually need to run the simulation with and without feedback
		# but for numerical work, x2 will suffice.
		return self.x2


class Weights(autoencoder.Weights):

	""" Contains the feedforward weights, feedback weights, and synaptic scaling parameters, as well as methods for plotting and updating them.

	Args:
			config: a ConfigParser.SafeConfigParser object

			nextdata: an example input stimulus, used to determine the number of neurons in the lower layer

	Attributes:
			W: 					The feedforward weights, before adding in the effects of the synaptic scaling. Size: n_x * n_z

			Wf: 					The feedback weights.  Typically the same as W, but in some experiments we modify it to be different. Size: n_x * n_z

			const_inh: 		A constant negative factor added to W when calculating Weff.  Used in some experiments to simulate the presence
																	of a pool of constantly active inhibitory neurons, which allows the feedforward weights W to remain strictly positive.

			theta: 				The vector of synaptic scaling constants. Size: n_z

			Weff:				The feedforward weights with the effects of synaptic scaling and constant inhibition included. Size: n_x * n_z

			Weffnonscale: The feedback weights with the effects of constand inhibition included. Size: n_x * n_z

			dW: 					The matrix of most recent changes to W.  Size: n_z * n_x .
																			(Note -- for historical reasons it's the transpose of what we might expect.)
	"""

	append_dict = {'pN':'number_of_weight_updates','W':'Wflat','Wf':'Wfflat','avg_firing_rates':'avg_firing_rates','theta':'theta'}
 
	transformMatrix = None


	@property
	def weights_table(self):
		weights_table = {
			"pN": UInt32Col(),
			"W": Float32Col(shape=(self.architecture.n_x * self.architecture.n_z)),
			"Wf": Float32Col(shape=(self.architecture.n_x * self.architecture.n_z)),
			"avg_firing_rates": FloatCol(shape=(self.architecture.n_z)),
			"theta": Float32Col(shape=(self.architecture.n_z)),
		}
		return weights_table

		

	
	def get_Wflat(self):
		return self.W.reshape(-1)

	def set_Wflat(self,value):
		self.W=value.reshape(np.shape(self.W))
		self.applyCalculatedVariables()

	Wflat = property(get_Wflat, set_Wflat)


	def get_Wfflat(self):
		return self.Wf.reshape(-1)

	def set_Wfflat(self,value):
		self.Wf=value.reshape(np.shape(self.Wf))
		self.applyCalculatedVariables()

	Wfflat = property(get_Wfflat, set_Wfflat)




	def calcWeff(self):
		""" The effective feedforward weights, including the effects of synaptic scaling (theta) and constant inhibition
		"""
		return self.W + self.theta - self.cv.const_inh

	def calcWeffnonscale(self):
		""" The effective feedback weights, including the effects of  constant inhibition
		"""
		if self.cv.const_inh == 0:
			return self.Wf
		else:
			return self.Wf - self.cv.const_inh

	def generateRandomWeights(self):
		if self.cv.gaussian_weights == 1:
			return np.random.normal(loc=0,size=(self.architecture.n_x, self.architecture.n_z)) * self.cv.initial_weight_scale + self.cv.const_inh
		elif self.cv.gaussian_weights == 2:
			return np.random.exponential(size=(self.architecture.n_x, self.architecture.n_z)) * self.cv.initial_weight_scale + self.cv.const_inh			
		else:
		#if self.config.runtype == 'iandf' or self.cv.min_W==0:
			return np.random.uniform(
				low=-0, high=1, size=(self.architecture.n_x, self.architecture.n_z)) * self.cv.initial_weight_scale + self.cv.const_inh


	def initializeWeights(self):
		""" Initialize the weights at the beginning of a simulation.

		W is initalized using random values from a uniform distribution from 0 - the config parameter initial_weight_scale,
		for integrate-and-fire simulations, or from (-1 - +1)*initial_weight_scale for all the other simulations.
		theta is initialized to the config parameter initial_theta_mean, plus normally distributed terms of width initial_theta_width.
		Wf is set to equal W.
		"""

		self.W = self.generateRandomWeights()

		self.Wf = self.W.copy()
		self.theta = np.ones((self.architecture.n_z)) * self.cv.initial_theta_mean + \
			np.random.normal(size=self.architecture.n_z) * \
			self.cv.initial_theta_width
		self.Weff = self.calcWeff()
		self.Weffnonscale = self.calcWeffnonscale()
		self.dW = np.zeros([self.architecture.n_z, self.architecture.n_x])
		self.number_of_weight_updates = 0
		self.avg_firing_rates = np.ones(self.architecture.n_z) * self.cv.initial_avg_firing_rates*self.cv.initial_avg_firing_rate_fraction
		if self.transformMatrix == None:
			self.setupTransformMatrix()

	def changeWeights(self, dW, dWf=[], dtheta=0):
		""" Update the weights and thetas, given desired change amoutns dW, dtheta etc, and do clipping.

		We have a separate function for this so that we can clip the weights to their max and min and keep
		track of how many weight updates there have been.
		"""
		if dWf == []:
			dWf = dW
		self.W += dW
		self.Wf += dWf
		self.W = np.clip(self.W, self.cv.min_W, self.cv.max_W)
		self.Wf = np.clip(self.Wf, self.cv.min_W, self.cv.max_W)
		self.theta += dtheta
		self.dW = dW
		self.number_of_weight_updates += 1
		self.applyCalculatedVariables()

	def applyCalculatedVariables(self):
		self.Weff = self.calcWeff()
		self.Weffnonscale = self.calcWeffnonscale()

	def applyConfigVariables(self, config, nextdata):
		""" Read the config object to set parameters relevant to the initialization of the weights.
		"""
		pass

	def showReceptiveFields(self, label='', sid=[], **kwarg):
		""" Plot a grid of squares where each square represents the incoming weights to a single hidden unit.
		"""
		W = self.W.copy()
		
		self.setupTransformMatrix(sid)
		pylab.imshow(self.transformW(W, **kwarg), cmap='gray')
		pylab.gca().get_xaxis().set_ticks([])
		pylab.gca().get_yaxis().set_ticks([])
		pylab.xlabel(label)


	def histTheta(self):
		""" Plot a histogram of synaptic scaling values (thetas) """
		h,bins=np.histogram(self.theta, 100)
		pylab.plot(bins[1:],h)
		pylab.title('Synaptic offsets for hidden units')

	def histW(self):
		""" Plot a pylab.histogram of the feedforward weights W"""
		h,bins=np.histogram(np.reshape(self.W, -1), 100)
		pylab.plot(bins[1:],h)
		
		pylab.title('Feedforward weights')

	def setupTransformMatrix(self, sid=[], combine=0):
		""" A utility function to pre-calculate a transformation that we use each time we display the weights.
		"""
		if self.architecture.n_x == 512 or self.architecture.n_x == 784 / 2 or self.architecture.n_x == 1568:  # this is the image patches
			n_x = int(self.architecture.n_x / 2)
			x_dims = (
				np.sqrt(self.architecture.n_x / 2), np.sqrt(self.architecture.n_x / 2))
		else:
			n_x = self.architecture.n_x
		xd1 = int(np.sqrt(n_x))

		x_dims = (xd1, xd1)
		n_z = min(100, self.architecture.n_z)
		x = np.arange(0, n_x * n_z, dtype=int)
		x = x.reshape(n_x, n_z)

		if sid == []:
			sid1 = int(np.sqrt(n_z))
			sid2 = sid1
		else:
			sid1 = sid[0]
			sid2 = sid[1]
		Wdisp = np.ones(
			[x_dims[0] * sid1 + 2 * sid1, x_dims[1] * sid2 + 2 * sid2], dtype=int) * -1
		for i in range(0, sid1):
			for j in range(0, sid2):
				ind = sid2 * i + j
				xs = x[:, ind]
				Wdisp[1 + (2 + x_dims[0]) * i:(2 + x_dims[0]) * (i + 1) - 1, 1 + (2 + x_dims[1])
					  * j:-1 + (2 + x_dims[1]) * (j + 1)] = np.reshape(xs, (x_dims[0], x_dims[1]))
		self.dispshape = np.shape(Wdisp)
		transformMatrix = Wdisp.reshape(-1)
		self.transformMatrixNonzero = (transformMatrix > -1).nonzero()
		self.transformMatrix = transformMatrix[self.transformMatrixNonzero]
		self.transformMatrixBlank = np.zeros(np.shape(transformMatrix))
		self.disp_nx = n_x


	# return	 the weights reshaped into a grid of images for easy visualization

	def weightsColumn(self,num_hid):
		self.setupTransformMatrix((num_hid,1))
		
		W=copy(self.W)
		return self.transformW(W)

	def transformW(self, W, normalize=1, combine='',weff=False):
		""" Reshape W into a grid of squares where each square represents the incoming weights to a single hidden unit.

		Returns reshaped W, ready for plotting.

		Args:

				W: a matrix of weights.  Only the weights to the first 100 hidden units will be plotted.

				normalize: 	If set to 1, the inputs to each hidden unit are rescaled to have min =0 and max=1.  If set to 2,
														the inputs to each hidden units are divided by the maximum of their absolute value.  If set to 0,
														no normalization is performed.
				combine: 		Used when visible units are divided into ON and OFF cells, as for natural images.  In this case, if combine =1,
														the output contains the values of W coming from the ON units minus the values of W coming from the OFF units. If
														combine=0, we don't do this.  If blank (the default), the program sets combine = 1 when the data are image patches.

		"""
		if weff:
			W*=self.theta
		W = W[:, 0:100]
		if combine == '':
			if self.config.combine:
				combine = 1
			else:
				combine = 0
# if self.architecture.n_x==512 or self.architecture.n_x==784/2: # we have image patches
	#				combine=1
		#		else:
		#		combine=0

		# inputs from both ON and OFF cells, subtracted together
		if combine == 1:
			W = W[0:self.disp_nx, :]-W[self.disp_nx:,:]
		elif combine == 2:  # just the ON cells
			W = W[0:self.disp_nx, :]
		elif combine == 3:  # just the OFF cells
			W = W[self.disp_nx:, :]

		if normalize == 1:
			W -= np.amin(W, axis=0)
			W /= np.amax(W, axis=0)
		elif normalize == 2:
			W /= np.amax(W, axis=0)

		W = W.reshape(-1)
		bb = self.transformMatrixBlank.copy()
		bb[self.transformMatrixNonzero] += np.take(W, self.transformMatrix)
		return bb.reshape(self.dispshape)


class WeightsPlussInhib(Weights):

	def initializeWeights(self):
		""" Initialize the weights at the beginning of a simulation.

		W is initalized using random values from a uniform distribution from 0 - the config parameter initial_weight_scale,
		for integrate-and-fire simulations, or from (-1 - +1)*initial_weight_scale for all the other simulations.
		theta is initialized to the config parameter initial_theta_mean, plus normally distributed terms of width initial_theta_width.
		Wf is set to equal W.
		"""

		self.W = self.generateRandomWeights()

		if self.cv.use_symmetric_weights == 1:
			self.Wf = self.W.copy()
		else:
			self.Wf= self.generateRandomWeights()
			
		self.theta = np.ones((self.architecture.n_z)) * self.cv.initial_theta_mean + \
			np.random.normal(size=self.architecture.n_z) * \
			self.cv.initial_theta_width
		self.Weff = self.calcWeff()
		self.Weffnonscale = self.calcWeffnonscale()

		self.Wex_to_inh = np.random.exponential(
				size=(self.architecture.n_m, self.architecture.n_z)) * self.cv.weight_to_inhib 

		self.Winh_to_ex = np.random.exponential(
				size=(self.architecture.n_z, self.architecture.n_m)) * self.cv.weight_from_inhib 

		self.Wff_to_inh = np.random.exponential(
				size=(self.architecture.n_m, self.architecture.n_x)) * self.cv.weight_ff_to_inhib 

		self.Wfb_to_inh = np.random.exponential(
				size=(self.architecture.n_mx, self.architecture.n_z)) * self.cv.weight_fb_to_inhib 


		self.Wx_to_inh_x = np.random.exponential(
				size=(self.architecture.n_mx, self.architecture.n_x)) * self.cv.weight_x_to_inhib 

		self.Winh_to_ex_x = np.random.exponential(
				size=(self.architecture.n_x, self.architecture.n_mx)) * self.cv.weight_x_from_inhib 



		self.dW = np.zeros([self.architecture.n_z, self.architecture.n_x])
		self.number_of_weight_updates = 0
		self.avg_firing_rates = np.ones(self.architecture.n_z) * self.cv.initial_avg_firing_rates*self.cv.initial_avg_firing_rate_fraction
		if self.transformMatrix == None:
			self.setupTransformMatrix()






class NonSymmetricWeights(Weights):
	def initializeWeights(self):
		""" Initialize the weights at the beginning of a simulation.

		W is initalized using random values from a uniform distribution from 0 - the config parameter initial_weight_scale,
		for integrate-and-fire simulations, or from (-1 - +1)*initial_weight_scale for all the other simulations.
		theta is initialized to the config parameter initial_theta_mean, plus normally distributed terms of width initial_theta_width.
		Wf is set to equal W.
		"""

		self.W = self.generateRandomWeights()
		self.Wf= self.generateRandomWeights()


		self.theta = np.ones((self.architecture.n_z)) * self.cv.initial_theta_mean + \
			np.random.normal(size=self.architecture.n_z) * \
			self.cv.initial_theta_width
		self.Weff = self.calcWeff()
		self.Weffnonscale = self.calcWeffnonscale()
		self.dW = np.zeros([self.architecture.n_z, self.architecture.n_x])
		self.number_of_weight_updates = 0
		self.avg_firing_rates = np.ones(self.architecture.n_z) * self.cv.initial_avg_firing_rates*self.cv.initial_avg_firing_rate_fraction
		if self.transformMatrix == None:
			self.setupTransformMatrix()


class Model(autoencoder.Model):

	name = 'Generic Two Layer Model'

	weights = Weights

	plots = Plots

	paramstring = '''

[simulation]

write_over_output_files = 1

inner_loop_size=1000
outer_loop_size=50
do_saves=1
max_presentations = 100000
runtype = rect 
# should be either rect, partrect, or iandf
datatype = filtered_Images_Dataset


initial_weight_scale_iandf = .00015 ; initial scale for the weights
experiment_name = default

background_mode = 0 ; run in background (so don't make plots)

number_test_images=100

first_image = 900


[weights]
initial_weight_scale = 0.005
const_inh = 0 ; the magnitude of the constant negative FF weight (this is then added to the excitatory weights that we learn. FB weights have same term but scaled according to FBfraction)
initial_theta_mean=0 ; initial mean value for theta, which determines 
# synaptic scaling (lower values mean neurons fire more easily)
initial_theta_width = 0.000 ; thetas are initially picked from a gaussian distribution with above mean and this width
initial_avg_firing_rates = 0.1
min_W = -10
max_W = 10
initial_avg_firing_rate_fraction = 0.8
gaussian_weights = 1

[plasticity]
use_binary_activity_for_theta = 0

learningRate=.01
thetaLearningRate=0.1
pp= 0.1 ; target number of spikes/presentation that each z neuron should fire
firing_rate_timescale = 500 ; this is the timescale (in numbers of presentations) over which we determine the average firing rate for calculating theta

weight_regularization = 0.0

[architecture]
n_z = 500

'''
