import os
import matplotlib
if ('IPython' in matplotlib.get_backend()) == False:
		matplotlib.use('Agg')
from autoencoder import simulation
from autoencoder import presentation
from autoencoder import datasets
from autoencoder import network
from autoencoder import save
from autoencoder import aeconfig
from autoencoder import analysis
from autoencoder import util
from autoencoder import defaultmodels
from autoencoder import batch
from autoencoder import figures


__version__ = '2.3'

class Architecture(object):

	def __init__(self,mydict):
		for key,val in mydict.items():
			setattr(self,key,val)



class Weights(object):

	architecture = None

	def __init__(self, config, architecture = None,refresh = False):
		""" Read config data and then initialize weights. """
		self.config = config
		self.cv=aeconfig.configView(self.config.configObject,sectionname='weights')
		self.cvi=aeconfig.configView(self.config.configObject,sectionname='weights',function_name='getint')

		if architecture is not None:
			self.architecture = architecture
		assert self.architecture is not None
		if refresh:
			self.calculated_variables_created = False

		# self.applyConfigVariables(config,nextdata)
		self.initializeWeights()

	def __getstate__(self):
		d = self.__dict__
		d['architecture']=self.architecture
		return d

	def __setstate__(self,d):
		self.__dict__ = d
		self.architecture = d['architecture']
		del self.architecture


	def initializeWeights(self):
		raise NotImplementedError()

	def updateAll(self, weights, P):
		raise NotImplementedError()

	def showReceptiveFields(self, label='', sid=[], **kwarg):
		raise NotImplementedError()

class Plasticity(object):

	calculated_variables_created = False

	architecture = None

	def __init__(self, config, architecture = None, refresh = False):
		if architecture is not None:
			self.architecture = architecture
		assert self.architecture is not None
		if refresh:
			self.calculated_variables_created = False

		self.config = config
		self.cv=aeconfig.configView(self.config.configObject,sectionname='plasticity')
		self.cvi=aeconfig.configView(self.config.configObject,sectionname='plasticity',function_name='getint')

		self._createCalculatedVariables()

	def _createCalculatedVariables(self):
		pass

	def updateAll(self, weights, P):
		raise NotImplementedError()


	def __getstate__(self):
		d = self.__dict__
		d['architecture']=self.architecture
		return d

	def __setstate__(self,d):
		self.__dict__ = d
		self.architecture = d['architecture']
		del self.architecture


class Plots(object):

	def __init__(self,net,analysis=None):
		self.net = net
		self.config = self.net.config
		self.architecture = self.net.architecture
		self.analysis = analysis


	def plotWeightHistory(self, numberTimes=5, sid=(2, 10)):
		raise NotImplementedError()

	def makePlots(self):
		raise NotImplementedError()





class Model(object):

	name = ''

	weights = None

	plots = None

	paramstring = ''

	def list_ancestors(self,object):
			mylist = [object]
			current = object
			while not current.__bases__ == ():
				current = current.__bases__[0]
				mylist.append(current)
			mylist = mylist[::-1]
			return mylist[1:]

	def collectParamStrings(self):
		
		mylist = self.list_ancestors(self.__class__)
		paramcombined = ''
		for j in mylist:
			paramcombined += j.paramstring + '\n'
		return paramcombined
