from __future__ import division
from pylab import  clf, plot, savefig,hist,subplot,tight_layout
from numpy import   mean,zeros,square,corrcoef
import numpy as np
from math import *
import pylab
import os
import cPickle as pickle
from cPickle import load
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as pyplot
import tables
from autoencoder.util import add_unique_postfix, Printer, make_sure_path_exists
from . import datasets
import ipdb



def gini_coef(list_of_values):

	if not any(list_of_values):
		return 0
	sorted_list = sorted(list_of_values)
	height, area = 0, 0
	for value in sorted_list:
		height += value
		area += height - value / 2.
		fair_area = height * len(list_of_values) / 2
	return (fair_area - area) / fair_area 



class Analysis(object):

	def __init__(self, net,plots):
		if isinstance(net, str):

			self.net = load(open(net, "r"))
			self.net.data = getattr(datasets, self.net.data)()
		else:
			self.net = net
		self.plots = plots
		self.config = self.net.config
		if hasattr(self.net.ModelClass,'numericalPresentation'):
			self.numericalPresentationInstance = self.net.ModelClass.numericalPresentation(self.config,architecture=self.net.architecture)
		else:
			self.numericalPresentationInstance = None

		self.time_plots = ('corrs','diffs','activationfractions','activations','highacts','ginis','lifetime_activations')

		self.time_plot_x_axis = 'pns'


	def addweightcorrs(self):
		print('Adding weight corrs')
		print('New version')
		pnmax=max(self.net.s.weightsList())
		z1s=self.net.s.presentationVariables('z1',condition='pN==%d'%(pnmax))
		if len(z1s)<1000:
			print('Adding %d more saved presentations'%(1000-len(z1s)))
			self.net.addSavedPresentations(1000-len(z1s))
			z1s=self.net.s.presentationVariables('z1',condition='pN==%d'%(pnmax))
		z1s=np.array(z1s).T
		n_z=self.net.architecture.n_z
		smaller_n_z=min(n_z,100)
		W=self.net.weights.W
		corrs=zeros((smaller_n_z,smaller_n_z))
		weightcorrs=zeros((smaller_n_z,smaller_n_z))
		for i in range(0,smaller_n_z):
			print i
			for j in range(0,i):
				corrs[i,j]=corrcoef(z1s[i],z1s[j])[0,1]
				corrs[j,i]=corrs[i,j]
				weightcorrs[i,j]=corrcoef(W[:,i],W[:,j])[0,1]
				weightcorrs[j,i]=weightcorrs[i,j]
		self.weightcorrs=weightcorrs
		self.spikecorrs=corrs

	
	def calcFBCorrs(self):
		self.ff_fb_corrs=[]
		pns = self.net.s.weightsList()
		currentWeights = None
		for pn in pns:
			Printer('Calculating corrs for pN = %d' % pn)
			currentWeights = self.net.s.weights(pN = pn,ww=currentWeights)[0]
			self.ff_fb_corrs.append(np.corrcoef(currentWeights.W.flatten(),currentWeights.Wf.flatten())[0,1])
		self.dump(self.outputname + '.pkl')

	def calcCorrs(self,factor=None):
		corrs = []
		factored_corrs=[]
		numcorrs = []
		diffs = []
		activationfractions = []
		activations = []
		lifetime_activations = []
		highacts = []
		ginis = []
		pns = self.net.s.weightsList()
		#pns=[1,100,1000,10000,100000]
		self.activation_deviations=[]
		numz = 0
		zactive = zeros(self.net.architecture.n_z)
		
		if self.numericalPresentationInstance is not None:
			rectP = self.numericalPresentationInstance
		numactcorx = []
		#numactcorz = []
		currentWeights = None
		for pn in pns:
			
			cor = []
			factored_cor=[]
			dif = []
			activFrac = []
			zactive*=0
			numpres = 0
			activ = []
			highact = []
			gini = []
			numcor=[]
			#if self.numericalPresentationInstance is not None:
			currentWeights = self.net.s.weights(pN = pn,ww=currentWeights)[0]
			
			with tables.open_file(self.net.s.filename, "r") as f:
				
				p_iterator=f.root.presentationTable.where('pN == %d'%pn, condvars=None, start=None, stop=None, step=None)
				for p_row in p_iterator:
					numpres += 1
					Printer('Calculating corrs for pN = %d and N = %d' % (pn,numpres))
					p= self.net.s.presentationFromRow(p_row,P=self.net.P)
					#if self.numericalPresentationInstance is not None:
					#	rectP.doPresentation(p.input_rates,currentWeights)
					#	numz = rectP.reconstructionCorr(currentWeights)
					#numaz,numax = p.numCorrs(currentWeights)
					z = p.reconstructionCorr(currentWeights)
					d = p.squaredError()
					cor.append(z)
					gini.append(gini_coef(p.z1[0:1000]))
					#numcor.append(numz)
					#numactcorx.append(corrcoef((p.get_x2()),rectP.x2)[0,1])
					#numactcorz.append(corrcoef((p.get_z1()),rectP.z1)[0,1])
					#numactcorz.append(numaz)
					zactive += mean(p.get_z1()>0)
					
					dif.append(d)
					activFrac.append(mean(p.z1 > 0))
					activ.append(mean(p.z1))
					highact.append(mean(p.z1 > self.net.config.configObject.getfloat('plasticity','pp')))
					if factor:
						factored_cor.append(p.reconstructionCorr(currentWeights,factor=factor))

			lifetime_activations.append(zactive/numpres)
			self.activation_deviations.append(square(zactive/numpres-self.net.plasticity.cv.pp))
			numcorrs.append(mean(numcor))
			corrs.append(mean(cor))
			if factor:
				factored_corrs.append(mean(factored_cor))
			diffs.append(mean(dif))
			activationfractions.append(mean(activFrac))
			activations.append(mean(activ))
			highacts.append(mean(highact))
			ginis.append(mean(gini))
		self.pns = pns
		self.corrs = corrs
		self.factored_corrs=factored_corrs
		self.diffs = diffs
		self.activationfractions = activationfractions
		self.activations = activations
		self.highacts = highacts
		self.ginis = ginis
		self.numcorrs = numcorrs
		self.numactcorx = numactcorx
		#self.numactcorz = numactcorz
		self.lifetime_activations=lifetime_activations
		self.dump(self.outputname + '.pkl')


	def calcNonCorrs(self,factor=None):
		# Just a quick fix for the case where we've accidentally saved activation levels that were calculating using scaled activations.
		corrs = []
		factored_corrs=[]
		numcorrs = []
		diffs = []
		activationfractions = []
		activations = []
		lifetime_activations = []
		highacts = []
		ginis = []
		pns = self.net.s.weightsList()
		#pns=[1,100,1000,10000,100000]
		self.activation_deviations=[]
		numz = 0
		zactive = zeros(self.net.architecture.n_z)
		
		if self.numericalPresentationInstance is not None:
			rectP = self.numericalPresentationInstance
		numactcorx = []
		#numactcorz = []
		currentWeights = None
		for pn in pns:
			
			cor = []
			factored_cor=[]
			dif = []
			activFrac = []
			zactive*=0
			numpres = 0
			activ = []
			highact = []
			gini = []
			numcor=[]
			#if self.numericalPresentationInstance is not None:
			currentWeights = self.net.s.weights(pN = pn,ww=currentWeights)[0]
			
			with tables.open_file(self.net.s.filename, "r") as f:
				
				p_iterator=f.root.presentationTable.where('pN == %d'%pn, condvars=None, start=None, stop=None, step=None)
				for p_row in p_iterator:
					numpres += 1
					Printer('Calculating corrs for pN = %d and N = %d' % (pn,numpres))
					p= self.net.s.presentationFromRow(p_row,P=self.net.P)
					#if self.numericalPresentationInstance is not None:
					#	rectP.doPresentation(p.input_rates,currentWeights)
					#	numz = rectP.reconstructionCorr(currentWeights)
					#numaz,numax = p.numCorrs(currentWeights)
					#numcor.append(numz)
					#numactcorx.append(corrcoef((p.get_x2()),rectP.x2)[0,1])
					#numactcorz.append(corrcoef((p.get_z1()),rectP.z1)[0,1])
					#numactcorz.append(numaz)
					zactive += mean(p.get_z1()>0)
					
					
					activFrac.append(mean(p.z1 > 0))
					activ.append(mean(p.z1))
					highact.append(mean(p.z1 > self.net.config.configObject.getfloat('plasticity','pp')))
					

			lifetime_activations.append(zactive/numpres)
			self.activation_deviations.append(square(zactive/numpres-self.net.plasticity.cv.pp))
			activationfractions.append(mean(activFrac))
			activations.append(mean(activ))
			highacts.append(mean(highact))
			ginis.append(mean(gini))
		self.pns = pns
		self.activationfractions = activationfractions
		self.activations = activations
		self.highacts = highacts
		self.ginis = ginis
		#self.numactcorz = numactcorz
		self.lifetime_activations=lifetime_activations
		self.dump(self.outputname + '.pkl')

	def plotGenerater(self):
		ylist = self.time_plots
		for y in ylist:
			yield (y,getattr(self,self.time_plot_x_axis),getattr(self,y))



	def makePNGs(self, modelname,outputname=''):

		if os.environ.has_key('AE_OUTPUT_DIR'):
			data_path = os.environ['AE_OUTPUT_DIR']
		else:
			import inspect
			#package_directory = os.path.dirname(os.path.abspath(inspect.getsourcefile(autoencoder)))
			package_directory = os.path.dirname(__file__)
			data_path = os.path.join(package_directory, '../../data/outputs')
#			data_path = self.__file__+'../data/'
			print 'Data output path is %s' % data_path





		make_sure_path_exists(os.path.join(data_path,self.config.analysis_output_dir))
		outputname = os.path.join(data_path,self.config.analysis_output_dir, self.config.experiment_name)
		if self.config.write_over_output_files == 0:
			outputname = add_unique_postfix(outputname, 'pdf')        
		print outputname
		clf()
		self.net.config.plotConfig(modelname=modelname)
		savefig(outputname + '_config.png', bbox_inches='tight')
		clf()
		self.net.weights.showReceptiveFields()
		savefig(outputname + '_receptive_fields.png', bbox_inches='tight')
		clf()
		if hasattr(self,'numericalPresentationInstance'):
			self.plots.plotReconstructionAttempts(numPresInstance=self.numericalPresentationInstance)
		else:
			self.plots.plotReconstructionAttempts()
		savefig(outputname + '_attempted_reconstructions.png',bbox_inches='tight')
		clf()

		G = self.plotGenerater()
		for y in G:
			clf()
			plot(y[1],y[2])
			savefig(outputname + '_'+y[0]+'.png', bbox_inches='tight')
		self.plots.plotWeightHistory()
		savefig(outputname + '_weight_history.png', bbox_inches='tight')
		clf()
		self.net.weights.histW()
		savefig(outputname + '_weights_histogram.png', bbox_inches='tight')
		clf()
		self.net.weights.histTheta()
		savefig(outputname + '_theta_histogram.png', bbox_inches='tight')
		if self.net.config.runtype == 'sigmoid':
			clf()
			self.net.weights.histThetaVisible()
			savefig(
				outputname + '_visible_theta_histogram.png', bbox_inches='tight')
		clf()

	def makeNowPlots(self,outputname='',modelname=''):

		if os.environ.has_key('AE_OUTPUT_DIR'):
			data_path = os.environ['AE_OUTPUT_DIR']
		else:
			import inspect
			#package_directory = os.path.dirname(os.path.abspath(inspect.getsourcefile(autoencoder)))
			package_directory = os.path.dirname(__file__)
			data_path = os.path.join(package_directory, '../../data/outputs')
#			data_path = self.__file__+'../data/'
			print 'Data output path is %s' % data_path


		make_sure_path_exists(os.path.join(data_path,self.config.analysis_output_dir))
		if outputname == '':
			outputname = os.path.join(data_path,self.config.analysis_output_dir, self.config.experiment_name)
			if self.config.write_over_output_files == 0:
				outputname = add_unique_postfix(outputname, 'pdf')        
#		print outputname

		pp = PdfPages(outputname + '.pdf.tmp')
		clf()
		self.net.weights.showReceptiveFields()
		pyplot.title('pN = %d  hidden unit receptive fields'%self.net.weights.number_of_weight_updates)
		pp.savefig()
		clf()
		self.net.config.plotConfig(modelname=modelname)
		pp.savefig()
		clf()
		self.net.P.rasterPlot()
		pp.savefig()
		clf()
		self.net.weights.histW()
		pp.savefig()
		clf()
		subplot(2,1,1)
		self.net.weights.histTheta()
		subplot(2,1,2)
		hist(self.net.weights.avg_firing_rates.reshape(-1), 100)
		tight_layout()
		pp.savefig()
		pp.close()
		os.rename(outputname+'.pdf.tmp',outputname+'.pdf')
		#self.dump(outputname + '.pkl')

	def makeAllPlots(self,outputname='',modelname=''):
		#ipdb.set_trace()   

		if os.environ.has_key('AE_OUTPUT_DIR'):
			data_path = os.environ['AE_OUTPUT_DIR']
		else:
			import inspect
			#package_directory = os.path.dirname(os.path.abspath(inspect.getsourcefile(autoencoder)))
			package_directory = os.path.dirname(__file__)
			data_path = os.path.join(package_directory, '../../data/outputs')
#			data_path = self.__file__+'../data/'
			print 'Data output path is %s' % data_path


		make_sure_path_exists(os.path.join(data_path,self.config.analysis_output_dir))
		if outputname == '':
			outputname = os.path.join(data_path,self.config.analysis_output_dir, self.config.experiment_name)
			if self.config.write_over_output_files == 0:
				outputname = add_unique_postfix(outputname, 'pdf')        
		print outputname
		self.outputname = outputname
	#	self.dump(outputname + '.pkl')
		pp = PdfPages(outputname + '.pdf')
		clf()
		self.net.weights.showReceptiveFields()
		pyplot.title('Final hidden unit receptive fields')
		pp.savefig()
		clf()
		self.net.config.plotConfig(modelname=modelname)
		pp.savefig()
		clf()
		self.plots.plotWeightHistory()
		pp.savefig()
		clf()
		self.plotCorrs()
		pp.savefig()
		clf()
		self.plotNumCorrs()
		pp.savefig()
		clf()
		self.plotDiffs()
		pp.savefig()
		clf()
		self.net.weights.histW()
		pp.savefig()
		clf()
		self.net.weights.histTheta()
		pp.savefig()
		if self.net.config.runtype == 'sigmoid':
			clf()
			self.net.weights.histThetaVisible()
			pp.savefig()
		clf()
		self.plotActivations()
		pp.savefig()
		clf()
		self.plotActivationFractions()
		pp.savefig()
		clf()
		self.plotHighActivationFractions()
		pp.savefig()
		clf()
		self.plotGinis()
		pp.savefig()
		clf()
		self.plotActivityCorrs()
		pp.savefig()
		clf()
		self.plotWeightCorrs()
		pp.savefig()
		clf()
		if hasattr(self,'numericalPresentationInstance'):
			self.plots.plotReconstructionAttempts(numPresInstance=self.numericalPresentationInstance)
		else:
			self.plots.plotReconstructionAttempts()
		pp.savefig()
		pp.close()

		self.dump(outputname + '.pkl')

	def plotDiffs(self):
		if not hasattr(self, 'diffs'):
			self.calcCorrs()
		plot(self.pns, self.diffs)
		pylab.xlabel('Number of training images')
		pylab.ylabel('Squared reconstruction error')

	def plotCorrs(self):
		if not hasattr(self, 'corrs'):
			self.calcCorrs()
		plot(self.pns, self.corrs)
		pylab.xlabel('Number of training images')
		pylab.ylabel('Correlation input, attempted reconstruction')

	def plotNumCorrs(self):
		if not hasattr(self, 'corrs'):
			self.calcCorrs()
		plot(self.pns, self.numcorrs)
		pylab.xlabel('Number of training images')
		pylab.ylabel('Correlation input, numerical reconstruction')



	def plotActivationFractions(self):
		if not hasattr(self,'activationfractions'):
		   self.calcCorrs()
		plot(self.pns,self.activationfractions)
		pylab.xlabel('Number of training images')
		pylab.ylabel('Average activation fraction of hidden units')
		#pass

	def plotActivations(self):
		if not hasattr(self, 'activations'):
			self.calcCorrs()
		plot(self.pns, self.activations)
		pylab.xlabel('Number of training images')
		pylab.ylabel('Average activation of hidden units')

	def plotHighActivationFractions(self):
		if not hasattr(self,'highacts'):
			self.calcCorrs()
		plot(self.pns,self.highacts)
		pylab.xlabel('Number of training images')
		pylab.ylabel('Average activation fraction > pp of hidden units')

	def plotGinis(self):
		if not hasattr(self,'ginis'):
			self.calcCorrs()
		plot(self.pns,self.ginis)
		pylab.xlabel('Number of training images')
		pylab.ylabel('Average Gini coefficients')


	def plotWeightCorrs(self):
		if not hasattr(self,'weightcorrs'):
			self.addweightcorrs()
		x = np.array(self.weightcorrs.reshape(-1))
		x  = x[~np.isnan(x)]
		hist(x,100,color='gray');
		#hold('True')
		#hist(self.weightcorrs.reshape(-1),100,color='gray');
		pylab.xlabel('Pearson correlation coefficient');
		pylab.ylabel('Number of pairs')

	def plotActivityCorrs(self):
		if not hasattr(self,'spikecorrs'):
			self.addweightcorrs()
		x = np.array(self.spikecorrs.reshape(-1))
		x  = x[~np.isnan(x)]
		hist(x,100,color='gray');
		#hold('True')
		#hist(self.weightcorrs.reshape(-1),100,color='gray');
		pylab.xlabel('Pearson correlation coefficient');
		pylab.ylabel('Number of pairs')


	def dump(self, filename):
		pickle.dump(self, open(filename, "wb"),-1)
