from __future__ import division
import numpy as np
from pylab import clf, subplot, plot, show, pause,ion
from numpy import mean
from math import *
from autoencoder.aeconfig import Config, configView
from time import time
import os

import matplotlib
import pylab

import autoencoder
import importlib

from .util import Printer



class Simulation(object):

	def __init__(self, model,configFileName='', configString = '', outputname='', runNow=False, **kwargs):
		#            self.savelist=np.concatenate((np.arange(0,100,1),np.arange(100,1000,100),np.arange(1000,10000,1000),np.arange(10000,100000,1000),np.arange(100000,500000,10000)))
		print configFileName
		self.configFileName = configFileName
		self.model = model

		# we collect paramstrings from the model and all its ancestors.  If the model redefines a parameter, it will be ok.
		self.config = Config(configFileName,configString=configString,paramstring=self.model().collectParamStrings(), **kwargs)
		self.cv=configView(self.config.configObject,sectionname='simulation')
		self.cvs=configView(self.config.configObject,sectionname='simulation',function_name='get')

		self.architecture = autoencoder.Architecture({'n_x':self.config.n_x,'n_z':self.config.configObject.getint('architecture','n_z')})
		if self.config.configObject.has_option('architecture','n_m'):
			self.architecture.n_m=self.config.configObject.getint('architecture','n_m')
                if self.config.configObject.has_option('architecture','n_mx'):
                        self.architecture.n_mx=self.config.configObject.getint('architecture','n_mx')
		self.net = autoencoder.network.Network(self.config,self.model,self.architecture,**kwargs)

		self.add_savelist()
	 	self.plots = self.model.plots(self.net)
		if self.cv.do_saves:
			self.s=self.net.s
			self.filename = self.s.filename
		else:
			self.net.P.dirname = ''
		if runNow:
			self.runAndAnalyze(outputname)

	


	def add_savelist(self):
		li=[]
		if self.cv.do_saves:
			for i in (1,10,100,1000,10000):
				li+=range(i,i*10,i)
			li+=range(150000,1000000,150000)
			
		else: # we are in test mode, show more frequently so we can observe
			li+=range(0,100)
			for i in (100,1000,10000):
				li+=range(i,i*10,int(i/10))

		li.append(100000)
		li.append(self.cv.max_presentations)
	
		self.savelist = np.unique(li)
		self.savelist=self.savelist[self.savelist<=self.cv.max_presentations]
		self.config.len_savelist = len(self.savelist)
	

	def runAndAnalyze(self, outputname):
		self.analysis = autoencoder.analysis.Analysis(self.net,self.plots)
		self.outputname=outputname
		self.run()
		self.net.addSavedPresentationsAllWeights(
			num=int(self.cv.number_test_images))
		if self.cv.do_saves:
			self.analysis.makeAllPlots(self.outputname,modelname=self.model.name)
		else:
			self.analysis.makeNowPlots(self.outputname,modelname=self.model.name)

	def run(self, max_presentations=[]):
		if max_presentations == []:
			max_presentations = self.cv.max_presentations
		max_presentations += self.net.weights.number_of_weight_updates
		net = self.net
		first_image = self.cv.first_image
		ion()
		while net.weights.number_of_weight_updates <= max_presentations:
			try:
				net.doPresentation()
				
#				if net.weights.number_of_weight_updates == self.cv.learningIncreaseTime:
#					mystr = str(self.cv.learningIncreaseValue)
#					self.net.config.configObject.set('plasticity','learningRate',mystr)
#					mystr = str(self.cv.learningThetaIncreaseValue)
#					self.net.config.configObject.set('plasticity','thetaLearningRate',mystr)


				if net.weights.number_of_weight_updates in self.savelist:
					
					if self.cv.do_saves:
						self.s.append(net.weights)
					if (net.weights.number_of_weight_updates > first_image)  & (self.cv.background_mode == 0):
						if 'IPython' in matplotlib.get_backend():
						#if os.environ.get('DISPLAY') is not None:
							self.plots.makePlots()
							
							show()
							pause(0.001)
							
						else:
							self.analysis.makeNowPlots(self.outputname,modelname=self.model.name)
				Printer(net.weights.number_of_weight_updates)
			except KeyboardInterrupt:
				self.RUNNING = False
				print "\n Program ended by user. \n"
				return self
			finally:
				pass

	def plotWeights(self, Nlist=(0, 1, 2, 100, 130)):
		clf()
		import locale
		locale.setlocale(locale.LC_ALL, 'en_US')
		locale.format("%d", 1255000, grouping=True)
		for i in xrange(0, len(Nlist)):
			subplot(ceil(len(Nlist) / 4), 4, i + 1)
			w = self.s.weights(N=Nlist[i],i=0)
			w.showReceptiveFields(label='N=%d' % w.number_of_weight_updates)

	def plotReconstructionCorrs(self, numberPresentations=100):
		clf()
		rs = []
		activationLevels = []
		ns = []
		net = self.savednet(0)
		print net.P.pp
		for i in xrange(0, self.numberInSavedNet()):
			net = self.savednet(i)
			net.addSavedPresentations(numberPresentations)
			corrs = []
			ys = []
			for j in xrange(0, numberPresentations):
				rc = net.savedPresentations[j].reconstructionCorr(net.weights)
				ys.append(net.savedPresentations[j].z1 > 0)
				if rc > 0:
					corrs.append(rc)
				else:
					corrs.append(0)
			rs.append(mean(corrs))
			activationLevels.append(np.mean(np.mean(ys, axis=0)))
			ns.append(net.P.presentation_number)
		subplot(1, 2, 1)
		oneminuscorrs = 1 - np.array(rs)
		plot(ns, oneminuscorrs)
		pylab.xlabel('Number of presentations')
		pylab.ylabel('1-Pearson correlation coefficient')
		subplot(1, 2, 2)
		diffs = net.P.pp - np.array(activationLevels)
		plot(ns, diffs)
		pylab.xlabel('Number of presentations')
		pylab.ylabel('Target - Average activation rate')
		show()
		return (ns,diffs,oneminuscorrs)

class SumatraSimulation(Simulation):

	def __init__(self,reason="reason for running this simulation",**kwarg):
		from sumatra.projects import load_project
		import os

		self.reason = reason
		os.path.dirname(__file__)
		# this is so that its call to git works correctly
		os.chdir(os.path.dirname(__file__))
		super(SumatraSimulation, self).__init__(kwarg)
		self.project = load_project(path=self.cvs.sumatradir)




	def run(self,*args,**kwargs):
		import cStringIO
		from sumatra.parameters import ConfigParserParameterSet

		output = cStringIO.StringIO()
		self.net.config.configObject.write(output)
		self.params = ConfigParserParameterSet(output.getvalue())
		output.close()

		self.record = self.project.new_record(parameters=self.params,
							reason=self.reason)
		self.net.config.configObject.set("shared","sumatra_label",self.record.label)

		start_time = time.time()
		Simulation.run(self,*args,**kwargs)
		self.record.duration = time.time() - start_time
		self.record.output_data = self.record.datastore.find_new_data(self.record.timestamp)
		self.project.add_record(self.record)

		self.project.save()

	
