from __future__ import division
import numpy as np
import pylab




class Weights(object):

	""" Contains the feedforward weights, feedback weights, and synaptic scaling parameters, as well as methods for plotting and updating them.

	Args:
			config: a ConfigParser.SafeConfigParser object

			nextdata: an example input stimulus, used to determine the number of neurons in the lower layer

	Attributes:
			W: 					The feedforward weights, before adding in the effects of the synaptic scaling. Size: n_x * n_z

			Wf: 					The feedback weights.  Typically the same as W, but in some experiments we modify it to be different. Size: n_x * n_z

			const_inh: 		A constant negative factor added to W when calculating Weff.  Used in some experiments to simulate the presence
																	of a pool of constantly active inhibitory neurons, which allows the feedforward weights W to remain strictly positive.

			theta: 				The vector of synaptic scaling constants. Size: n_z

			Weff:				The feedforward weights with the effects of synaptic scaling and constant inhibition included. Size: n_x * n_z

			Weffnonscale: The feedback weights with the effects of constand inhibition included. Size: n_x * n_z

			dW: 					The matrix of most recent changes to W.  Size: n_z * n_x .
																			(Note -- for historical reasons it's the transpose of what we might expect.)
	"""

	def calcWeff(self):
		""" The effective feedforward weights, including the effects of synaptic scaling (theta) and constant inhibition
		"""
		return self.W + self.theta / self.config.n_x - self.config.const_inh

	def calcWeffnonscale(self):
		""" The effective feedback weights, including the effects of  constant inhibition
		"""
		if self.config.const_inh == 0:
			return self.Wf
		else:
			return self.Wf - self.config.const_inh

	def __init__(self, config, nextdata=[]):
		""" Read config data and then initialize weights. """
		self.config = config
		# self.applyConfigVariables(config,nextdata)
		self.initializeWeights()

	def initializeWeights(self):
		""" Initialize the weights at the beginning of a simulation.

		W is initalized using random values from a uniform distribution from 0 - the config parameter initial_weight_scale,
		for integrate-and-fire simulations, or from (-1 - +1)*initial_weight_scale for all the other simulations.
		theta is initialized to the config parameter initial_theta_mean, plus normally distributed terms of width initial_theta_width.
		Wf is set to equal W.
		"""
		if self.config.runtype == 'iandf':
			self.W = np.random.uniform(
				low=-0, high=1, size=(self.config.n_x, self.config.n_z)) * self.config.initial_weight_scale + self.config.const_inh
		else:
			self.W = np.random.uniform(
				low=-1, high=1, size=(self.config.n_x, self.config.n_z)) * self.config.initial_weight_scale + self.config.const_inh

		self.Wf = self.W.copy()
		self.theta = np.ones((self.config.n_z)) * self.config.initial_theta_mean + \
			np.random.normal(size=self.config.n_z) * \
			self.config.initial_theta_width
		self.Weff = self.calcWeff()
		self.Weffnonscale = self.calcWeffnonscale()
		self.dW = np.zeros([self.config.n_z, self.config.n_x])
		self.number_of_weight_updates = 0
		self.avg_firing_rates = np.ones(self.config.n_z) * self.config.pp * self.config.initial_avg_firing_rate_fraction
		self.setupTransformMatrix()

	def changeWeights(self, dW, dWf=[], dtheta=0):
		""" Update the weights and thetas, given desired change amoutns dW, dtheta etc, and do clipping.

		We have a separate function for this so that we can clip the weights to their max and min and keep
		track of how many weight updates there have been.
		"""
		if dWf == []:
			dWf = dW
		self.W += dW
		self.Wf += dWf
		self.W = np.clip(self.W, self.config.min_W, self.config.max_W)
		self.Wf = np.clip(self.Wf, self.config.min_W, self.config.max_W)
		self.theta += dtheta
		self.dW = dW
		self.number_of_weight_updates += 1
		self.applyCalculatedVariables()

	def applyCalculatedVariables(self):
		self.Weff = self.calcWeff()
		self.Weffnonscale = self.calcWeffnonscale()

	def applyConfigVariables(self, config, nextdata):
		""" Read the config object to set parameters relevant to the initialization of the weights.
		"""
		pass

	def showReceptiveFields(self, label='', sid=[], **kwarg):
		""" Plot a grid of squares where each square represents the incoming weights to a single hidden unit.
		"""
		W = self.W.copy()
		if sid != []:
			self.setupTransformMatrix(sid)
		out = self.transformW(W, **kwarg)
		pylab.imshow(out, cmap='gray')
		pylab.gca().get_xaxis().set_ticks([])
		pylab.gca().get_yaxis().set_ticks([])
		pylab.xlabel(label)
		if sid != []:
			self.setupTransformMatrix()

		

	def histTheta(self):
		""" Plot a histogram of synaptic scaling values (thetas) """
		pylab.hist(self.theta, 100)
		pylab.title('Synaptic offsets for hidden units')

	def histW(self):
		""" Plot a pylab.histogram of the feedforward weights W"""
		pylab.hist(np.reshape(self.W, -1), 100)
		pylab.title('Feedforward weights')

	def setupTransformMatrix(self, sid=[], combine=0):
		""" A utility function to pre-calculate a transformation that we use each time we display the weights.
		"""
		if self.config.n_x == 512 or self.config.n_x == 784 / \
				2:  # this is the image patches
			n_x = int(self.config.n_x / 2)
			x_dims = (
				np.sqrt(self.config.n_x / 2), np.sqrt(self.config.n_x / 2))
		else:
			n_x = self.config.n_x
		xd1 = int(np.sqrt(n_x))

		x_dims = (xd1, xd1)
		n_z = min(100, self.config.n_z)
		x = np.arange(0, n_x * n_z, dtype=int)
		x = x.reshape(n_x, n_z)

		if sid == []:
			sid1 = int(np.sqrt(n_z))
			sid2 = sid1
		else:
			sid1 = sid[0]
			sid2 = sid[1]
		Wdisp = np.ones(
			[x_dims[0] * sid1 + 2 * sid1, x_dims[1] * sid2 + 2 * sid2], dtype=int) * -1
		for i in range(0, sid1):
			for j in range(0, sid2):
				ind = sid2 * i + j
				xs = x[:, ind]
				Wdisp[1 + (2 + x_dims[0]) * i:(2 + x_dims[0]) * (i + 1) - 1, 1 + (2 + x_dims[1])
					  * j:-1 + (2 + x_dims[1]) * (j + 1)] = np.reshape(xs, (x_dims[0], x_dims[1]))
		self.dispshape = np.shape(Wdisp)
		transformMatrix = Wdisp.reshape(-1)
		self.transformMatrixNonzero = (transformMatrix > -1).nonzero()
		self.transformMatrix = transformMatrix[self.transformMatrixNonzero]
		self.transformMatrixBlank = np.zeros(np.shape(transformMatrix))
		self.disp_nx = n_x

	# return	 the weights reshaped into a grid of images for easy visualization
	def transformW(self, W, normalize=1, combine='',weff=0):
		""" Reshape W into a grid of squares where each square represents the incoming weights to a single hidden unit.

		Returns reshaped W, ready for plotting.

		Args:

				W: a matrix of weights.  Only the weights to the first 100 hidden units will be plotted.

				normalize: 	If set to 1, the inputs to each hidden unit are rescaled to have min =0 and max=1.  If set to 2,
														the inputs to each hidden units are divided by the maximum of their absolute value.  If set to 0,
														no normalization is performed.
				combine: 		Used when visible units are divided into ON and OFF cells, as for natural images.  In this case, if combine =1,
														the output contains the values of W coming from the ON units minus the values of W coming from the OFF units. If
														combine=0, we don't do this.  If blank (the default), the program sets combine = 1 when the data are image patches.

		"""
		if weff:
			W*=self.theta
		
		W = W[:, 0:100]
		if combine == '':
			if self.config.combine:
				combine = 1
			else:
				combine = 0
# if self.config.n_x==512 or self.config.n_x==784/2: # we have image patches
	#				combine=1
		#		else:
		#		combine=0

		# inputs from both ON and OFF cells, subtracted together
		if combine == 1:
			W = W[0:self.disp_nx, :]-W[self.disp_nx:,:]
		elif combine == 2:  # just the ON cells
			W = W[0:self.disp_nx, :]
		elif combine == 3:  # just the OFF cells
			W = W[self.disp_nx:, :]

		if normalize == 1:
			W -= np.amin(W, axis=0)
			W /= np.amax(W, axis=0)
		elif normlize == 2:
			W /= np.amax(W, axis=0)

		W = W.reshape(-1)
		bb = self.transformMatrixBlank.copy()
		bb[self.transformMatrixNonzero] += np.take(W, self.transformMatrix)
		return bb.reshape(self.dispshape)


class WeightsPlusVisibleBias(Weights):

	""" Weights class that includes

	"""

	def __init__(self, config, nextdata=[]):
		super(WeightsPlusVisibleBias, self).__init__(config, nextdata)
		self.W = np.random.uniform(low=0, high=1, size=(
			self.config.n_x, self.config.n_z)) * self.config.initial_weight_scale + self.config.const_inh
		self.Wf = self.W.copy()
		self.thetaVisible = np.zeros(self.config.n_x)

	def changeWeights(self, dW, dWf=[], dtheta=(0, 0)):
		if dWf == []:
			dWf = dW
		self.W += dW
		self.Wf += dWf
		self.W = np.clip(self.W, self.config.min_W, self.config.max_W)
		self.Wf = np.clip(self.Wf, self.config.min_W, self.config.max_W)
		self.theta += dtheta[0]
		self.thetaVisible += dtheta[1]
		self.dW = dW
		self.number_of_weight_updates += 1

	def histThetaVisible(self):
		""" Plot a histogram of synaptic scaling values (thetas) """
		pylab.hist(self.thetaVisible, 100)
		pylab.title('Synaptic offsets for visible units')
