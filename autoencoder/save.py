from __future__ import division
import numpy as np
from math import *
import os
import tables
import time
import pdb


paramstring = '''

experiment_name = string(default = "")
'''

class Save():

	def __init__(
			self, config, presentation_instance,weights_instance,runname='', dirname='', number=0,architecture=None):
		""" Create and initialize a data file (and potentially a directory) in which to save the outputs of  a simulation.

		This function creates a tables (hdf5) file for storing simulation results.  It creates a variable length array in which objects (like the network object?)
		are stored periodically during the simulation run.  Additionally, it saves as a string a copy of every .py file found in the directory where the file
		is to be saved.  This way if we are running the code from the same directory, we have a saved copy of exaclty what code was run.

		The function optionally creates a directory  for saving the file into.  (Obviously in this case there won't be any .py files found!)

		Args:
				runname: A string to be appended to the name of the created directory, if one is created.
				dirname: A path of a directory telling where to store the outputs. Should be of the form /ga/wallace/Data .
												If not specified, a directory will be created in ~/Data/ , and the name will depend on the current time
												plus "runname" appended to the end.
				filename: The name of the output file (such as 'output').
				number: A number to be appended to the output file name.  Use to keep track of multiple simulations in the same directory.

		"""
		# os.environ['AE_SAVE_DIR']=os.path.expanduser("~")
		self.config = config
 		self.architecture = architecture
 		self.weightsListSaved = []

		if dirname == '':
			shortdirname = (time.strftime("%Y%m%d-%H%M%S"))

			max_retries = 3
			retries = 0


			if os.environ.has_key('AE_SAVE_DIR'):
				data_path = os.environ['AE_SAVE_DIR']
			else:
				import inspect
				#package_directory = os.path.dirname(os.path.abspath(inspect.getsourcefile(autoencoder)))
				package_directory = os.path.dirname(__file__)
				data_path = os.path.join(package_directory, '../../data/saved')
	#			data_path = self.__file__+'../data/'
				print 'Data saving path is %s' % data_path


			savepath = os.path.join(data_path, shortdirname)

			while (os.path.isdir(savepath) == False) and retries < max_retries:

				try:
					retries += 1
					os.mkdir(savepath)
				except:
					print 'Tried and failed to make directory %s' %savepath
					#pass
			assert os.path.isdir(savepath)
		
		print savepath

		self.filename = os.path.join(savepath,self.config.experiment_name + str(number) + '.hdf5' )
		with tables.open_file(self.filename, "w") as f:
			filters = tables.Filters(
				complevel=5, complib='zlib', fletcher32=False)
			#f.createVLArray(f.root, 'weightObjects', tables.ObjectAtom(
			#), title='Weight objects', filters=filters)
			f.create_table(f.root, 'weightTable', weights_instance.weights_table,
						   title="Saved table of weights",filters=filters,expectedrows=self.config.len_savelist)
			mytable=presentation_instance.presentation_table
			f.create_table(f.root,'presentationTable',mytable,title="Saved table of presentations",filters=filters,expectedrows=self.config.number_test_images*self.config.len_savelist)

			f.createVLArray(
				f.root, 'codeObjects', tables.ObjectAtom(), title='Source code strings')
			
		self.dirname = dirname

	def append(self, object):

		#pdb.set_trace()
		if isinstance(object, self.config.model.presentation):
			with tables.open_file(self.filename, "a") as f:
				mytable = f.root.presentationTable
				newrow = mytable.row
				self.setup_append(object,newrow)
				newrow.append()
				mytable.flush()

		elif isinstance(object, self.config.model.weights):

			# if self.weights(pN=object.number_of_weight_updates):
			#       warnings.warn('We already have these weights stored. Not storing them again.')
			with tables.open_file(self.filename, "a") as f:
				mytable = f.root.weightTable
				newrow = mytable.row
				self.setup_append(object,newrow)
				newrow.append()
				mytable.flush()
			self.weightsListSaved.append(object.number_of_weight_updates)
				# f.root.weightObjects.append(object)
		else:
			raise Exception("I don't know how to save this type of object")

	def setup_append(self,object,newrow):
		for key, val in object.append_dict.items():
			newrow[key]=getattr(object,val)


	def fillObjectFromRow(self,obj,inrow):
		for key,val in obj.append_dict.items():
			setattr(obj,val,inrow[key])
			#pass
		return obj

		

	def weightsFromRow(self,inrow,ww=None):
		if ww == None:
			ww = self.config.model.weights(self.config,architecture=self.architecture)
		ww= self.fillObjectFromRow(ww,inrow)
		ww.applyCalculatedVariables()
		return ww



	def presentationFromRow(self, inrow,P=None):
		if P==None:
			P = self.config.model.presentation(self.config,architecture=self.architecture)
		return self.fillObjectFromRow(P,inrow)

	def weights(self, N=[], pN=[],ww=None,i=None):
		# optionally imput ww as a weights object to fill
		with tables.open_file(self.filename, "r") as f:
			if (N == []) & (pN == []):
				return []
			elif pN == []:
				return self.weightsFromRow(f.root.weightTable[N],ww=ww)
			elif N == []:
				if i is not None:
					result = self.weightsFromRow(f.root.weightTable.read_where('pN == %d' % pN)[i])
				else:

					result = [self.weightsFromRow(
						row,ww=ww) for row in f.root.weightTable.read_where('pN == %d' % pN)]
				return result
			f.root.weightTable.flush()


	def presentationVariables(self,variable,condition='pN==pN'):
		P = self.config.model.presentation(self.config)
		column_name_from_variable = P.append_dict[variable]
		with tables.open_file(self.filename, "r") as f:
			passvalues = [ row[column_name_from_variable] for row in  f.root.presentationTable.where(condition)]
		return passvalues

	def presentations(self, N=[], pN=[],i=None):

		with tables.open_file(self.filename, "r") as f:
			if (N == []) & (pN == []):
				result = []
			elif pN == []:
					 # we
				if f.root.weightObjects.nrows <= N:
					result = []
				else:
					pN = f.root.weightObjects[N].number_of_weight_updates
					result = [
						x for x in f.root.presentationTable.iterrows() if x.weight_update_number == pN]
			elif N == []:
				if i is not None:
					result = self.presentationFromRow(f.root.presentationTable.read_where('pN == %d' % pN)[i])
				else:
					result = [self.presentationFromRow(
						row) for row in f.root.presentationTable.read_where('pN == %d' % pN)]
		return result


	def weightsList(self):
		if self.weightsListSaved == []:
			with tables.open_file(self.filename, "r") as f:
				num = f.root.weightTable.read(field='pN')
			#f.root.weightTable.flush()
			self.weightsListSaved = np.unique(num)
		return self.weightsListSaved

	def presentationsList(self):
		outs = []
		with tables.open_file(self.filename, "r") as f:
			num = f.root.presentationTable.read(field='pN')
			f.root.presentationTable.flush()
			num = np.unique(num)
		for j in num:
			outs.append((j, len(self.presentations(pN=j))))
		return outs

	# def __repr__(self):
	#   return 'Saved weights:\r'+tabulate(np.asarray([range(0,len(self.weightsList())),self.weightsList()]).T,('Number in list (N)','Weight update numbers (pN)'))\
	#   +'\r\r Saved presentations:\r'+tabulate(self.presentationsList(),('Weight update number (pN)','Number of saved presentations'))
