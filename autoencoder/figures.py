import numpy as np
import matplotlib.pylab as plt
import prettyplotlib as ppl
from os.path import expanduser
import pylab as plt
from matplotlib.ticker import MaxNLocator,FixedLocator
import matplotlib.ticker as mticker
from matplotlib import rc
from pylab import clf, figure, subplot,Polygon,text,plot,xlabel,ylabel,legend,ylim,figure
import matplotlib
from wand.image import Image as WImage
from os import path
import inspect


class Figures(object):

	def __init__(self, an=None,dataname=None,figure_path = '/Dropbox/WorkDocuments/PaperDrafts/autoencoder/figures/',
			pNs=[1,10000,100000],factor=None,presnumber=0):
		home = expanduser("~")
		self.figure_path = home + figure_path
		self.imgs={}
		self.an = an
		self.dataname = dataname
		self.pNs=pNs
		self.factor=factor
		self.presnumber=presnumber
		if an:		
			if not self.dataname:
				self.dataname=path.basename(self.an.net.s.filename)[:-5]
		else:
			if not self.dataname:
				self.dataname='generic'

	def classMethods(the_class,class_only=False,instance_only=False,exclude_internal=True):

		def acceptMethod(tup):
			#internal function that analyzes the tuples returned by getmembers tup[1] is the 
			#actual member object
			is_method = inspect.ismethod(tup[1])
			if is_method:
				bound_to = tup[1].im_self
				internal = tup[1].im_func.func_name[:2] == '__' and tup[1].im_func.func_name[-2:] == '__'
				if internal and exclude_internal:
					include = False
				else:
					include = (bound_to == the_class and not instance_only) or (bound_to == None and not class_only)
			else:
				include = False
			return include
		#uses filter to return results according to internal function and arguments
		return filter(acceptMethod,inspect.getmembers(the_class.__class__))

	def makeAll(self):
		methods=self.classMethods()
		plotMethods=[]
		for i in range(0,len(methods)):
			if 'Plot' in methods[i][0]:
				# run it
				
				methods[i][1](self)

	def add_file_to_save(self,name,bbox_inches='tight'):
		plt.savefig(self.figure_path+name+'.pdf',bbox_inches=bbox_inches,pad_inches=0.05)
		print('Saved '+self.figure_path+name+'.pdf')
		self.imgs[name]=WImage(filename=self.figure_path+name+'.pdf')

	def cartoonRasterPlot(self,figsize=(8,4)):

		def add_range_annotation(ax, start, end, txt_str, y_height=.5,y_offset=2, x_offset=0,txt_kwargs=None, arrow_kwargs=None):
			"""
			Adds horizontal arrow annotation with text in the middle

			Parameters
			----------
			ax : matplotlib.Axes
				The axes to draw to

			start : float
				start of line

			end : float
				end of line

			txt_str : string
				The text to add

			y_height : float
				The height of the line

			txt_kwargs : dict or None
				Extra kwargs to pass to the text

			arrow_kwargs : dict or None
				Extra kwargs to pass to the annotate

			Returns
			-------
			tuple
				(annotation, text)
			"""

			if txt_kwargs is None:
				txt_kwargs = {}
			if arrow_kwargs is None:
				# default to your arrowprops
				arrow_kwargs = {'arrowprops':dict(arrowstyle="<->",
									connectionstyle="bar",
									ec="k",
									shrinkA=5, shrinkB=5,
									)}

			trans = ax.get_xaxis_transform()
			(x,y)=range_brace(start,end,height=y_height)
			ax.plot(x, y+y_offset,'-',clip_on=False,color='gray')
			txt = ax.text((start + end)*.3+x_offset,
						  y_height + y_offset+.05,
						  txt_str,
						  **txt_kwargs)
			if plt.isinteractive():
				plt.draw()
			return txt

		def range_brace(x_min, x_max, mid=0.75, beta1=50.0, beta2=100.0, height=1, initial_divisions=11, resolution_factor=1.5):
			import numpy as NP
			x0 = NP.array(())
			tmpx = NP.linspace(0, 0.5, initial_divisions)
			tmp = beta1**2 * (NP.exp(beta1*tmpx)) * (1-NP.exp(beta1*tmpx)) / NP.power((1+NP.exp(beta1*tmpx)),3)
			tmp += beta2**2 * (NP.exp(beta2*(tmpx-0.5))) * (1-NP.exp(beta2*(tmpx-0.5))) / NP.power((1+NP.exp(beta2*(tmpx-0.5))),3)
			for i in range(0, len(tmpx)-1):
				t = int(NP.ceil(resolution_factor*max(NP.abs(tmp[i:i+2]))/float(initial_divisions)))
				x0 = NP.append(x0, NP.linspace(tmpx[i],tmpx[i+1],t))
			x0 = NP.sort(NP.unique(x0)) # sort and remove dups
			# half brace using sum of two logistic functions
			y0 = mid*2*((1/(1.+NP.exp(-1*beta1*x0)))-0.5)
			y0 += (1-mid)*2*(1/(1.+NP.exp(-1*beta2*(x0-0.5))))
			# concat and scale x
			x = NP.concatenate((x0, 1-x0[::-1])) * float((x_max-x_min)) + x_min
			y = NP.concatenate((y0, y0[::-1])) * float(height)
			return (x,y)

		def raster(event_times_list, color='k',limits=[],specialtime=[],specialcolors=[],specialstyles=[]):
			"""
			Creates a raster plot
			Parameters
			----------
			event_times_list : iterable
							   a list of event time iterables
			color : string
					color of vlines
			Returns
			-------
			ax : an axis containing the raster plot
			"""
			my_locator = FixedLocator((1,2,3,4))

			ax = plt.gca()
			for ith, trial in enumerate(event_times_list):
				for t in trial:
					if t in specialtime:
						
						plt.vlines(t, ith + .6, ith + 1.4, color=specialcolors[ith],linestyle=specialstyles[ith],linewidth=1.5)
					else:    
						plt.vlines(t, ith + .6, ith + 1.4, color=color,linewidth=1.5)
			plt.ylim(.5, len(event_times_list) + .5)
			ax.spines['right'].set_visible(False)
			ax.spines['top'].set_visible(False)
			ax.tick_params(right='off',top='off')
			ax.yaxis.set_major_locator( my_locator )
			ax.xaxis.set_major_locator( MaxNLocator(4))
			if limits != []:
				plt.xlim(limits)
			#ax.yaxis.set_ticklabels(('Neuron 1','Neuron 2'))
			return ax


		fig=figure(figsize=figsize)
		# example usage
		# Generate test data
		nbins = 100
		ntrials = 10
		spikes = []
		#for i in range(ntrials):
		#    spikes.append(np.arange(nbins)[np.random.rand(nbins) < .3])
		#spikes=((1,2,3,12,13,14),(4,6,8))
		offset=7
		spikes=((0,1,2,3,4,1+2*offset,3+2*offset),(8,9,10))


	   # title('Example raster plot')
		ax = raster(spikes,limits=(-2,max(max(spikes[0]),max(spikes[1]))+2),specialtime=(1,9),specialcolors=('k','k'),specialstyles=('dashdot','dashdot'))
		ax.set_yticklabels(('Visible\n neuron $i$','Hidden\n neuron $j$'),rotation='horizontal',size=20)
		#ax.set_xticks((1,9))
		#ax.set_xticklabels(('$t_{k}$','$t_{l}$'),size=20)
		ax.set_xticks((0,10,18))
		ax.set_xticklabels(('0','30','60'))
		ax.text(0.8,.4,('Stimulus on'))
		plt.xlabel(r'Time (ms) $\longrightarrow$',size=20)
		plt.hlines(0.5,-.2,4.2,colors='gray',linewidth=8)
		#plt.hlines(2.5,-.2,4.2,colors='gray',linewidth=8)
		ax.text(0.5,1.5,r'Initial bout')
		ax.text(6.5,1.5,r'Intermediate bout')
		ax.text(14.5,1.7,r'Final bout')
		ax.text(14,1.5,r'(attempted reconstruction)')
		add_range_annotation(ax, 2, 9, r'$\Delta t_1$',y_height=.2,x_offset=2,y_offset=2.5)
		add_range_annotation(ax, 9, 16, r'$\Delta t_2$',y_height=.2,x_offset=4.5,y_offset=2.5)
		self.add_file_to_save('rasterplot')
		plt.show()


	def cartoonNetworkPlot(self,figsize=(6,4)):
		
		fig = plt.figure(facecolor='w',figsize=figsize)
		ax = fig.add_axes([0, 0, 1, 1],
						  xticks=[], yticks=[])
		plt.box(False)
		circ = plt.Circle((1, 1), 2)

		radius = 0.3

		arrow_kwargs = dict(head_width=0.05, fc='black')


		# function to draw arrows
		def draw_connecting_arrow(ax, circ1, rad1, circ2, rad2):
			theta = np.arctan2(circ2[1] - circ1[1],
							   circ2[0] - circ1[0])

			starting_point = (circ1[0] + rad1 * np.cos(theta),
							  circ1[1] + rad1 * np.sin(theta))

			length = (circ2[0] - circ1[0] - (rad1 + 1.4 * rad2) * np.cos(theta),
					  circ2[1] - circ1[1] - (rad1 + 1.4 * rad2) * np.sin(theta))

			ax.arrow(starting_point[0], starting_point[1],
					 length[0], length[1],**arrow_kwargs)



		# function to draw circles
		def draw_circle(ax, center, radius,text='1'):
			circ = plt.Circle(center, radius, fc='none', lw=2)
			ax.add_patch(circ)
			ax.text(center[0]-radius/2.5,center[1]-radius/4,text)
		x1 = -2
		x2 = 0
		x3 = 2
		y3 = 0

		#------------------------------------------------------------
		# draw circles

		y2s = np.linspace(1.5,-1.5,4)

		def ifthen(i,maxval,replace):
			if i<maxval:
				return i
			else:
				return replace
		for i, y1 in enumerate(np.linspace(1.5, -1.5, 4)):
			draw_connecting_arrow(ax, (x1 - 0.9, y1), 0.1, (x1, y1), radius)
		 
			draw_circle(ax, (x1, y1), radius,text=r'$x_%s$' % ifthen(i+1,4,'N'))
			ax.text(x1 - 0.9, y1, 'Input Pixel %s' % ifthen(i + 1,4,'N'),
					ha='right', va='center', fontsize=12)
		ax.text(x1 - 0.9, y1+0.7,'...',ha='center', va='center', fontsize=36)

			
			
		for i, y2 in enumerate(y2s):
			draw_circle(ax, (x2, y2), radius,text=r'$y_%s$'%ifthen(i+1,4,'M'))

		#draw_circle(ax, (x3, y3), radius)
		#ax.text(x3 + 0.8, y3, 'Output', ha='left', va='center', fontsize=16)
		#draw_connecting_arrow(ax, (x3, y3), radius, (x3 + 0.8, y3), 0.1)

		#------------------------------------------------------------
		# draw connecting arrows
		for y1 in np.linspace(-1.5, 1.5, 4):
			for y2 in y2s:
				draw_connecting_arrow(ax, (x1, y1), radius, (x2, y2), radius)
				draw_connecting_arrow(ax, (x2, y2), radius, (x1, y1), radius)
		#        draw_connecting_arrow(ax, (x2, y2), radius, (x1, y1), radius)

		#for y2 in np.linspace(-2, 2, 5):
		#    draw_connecting_arrow(ax, (x2, y2), radius, (x3, y3), radius)

		#------------------------------------------------------------
		# Add text labels
		plt.text(x1, 2.25, "Lower Layer", ha='center', va='top', fontsize=16)
		plt.text(x2, 2.25, "Upper Layer", ha='center', va='top', fontsize=16)
		#plt.text(x3, 2.7, "Output\nLayer", ha='center', va='top', fontsize=16)

		ax.set_aspect('equal')
		plt.xlim(-4.5, 1)
		plt.ylim(-2, 2.5)
		self.add_file_to_save('cartoon_network')
		plt.show()
		
		


	def synapticPlasticityPlot(self,figsize=(2.0, 2.2)):
		figure(figsize=figsize)
		x=np.linspace(-150,150,200)
		spot=25
		def stdprule(x,ltd_amp,ltd_tau,ltp_amp,ltp_tau):
			return ltd_amp*np.exp(x/ltd_tau)*(x<0)+ltp_amp*np.exp(-x/ltp_tau)*(x>0)

		def doPlot(x,y):
			plt.clf()
			rc('figure',figsize=figsize)
			ax=plt.subplot(1,1,1)
			
			ax.spines['right'].set_visible(False)
			ax.spines['top'].set_visible(False)
			ax.tick_params(right='off',top='off')
			myLocator = mticker.MultipleLocator(100)
			ax.xaxis.set_major_locator(myLocator)
			plt.axhline(0,color='black',linewidth=0.75)
			plt.plot(x,y,color='black',zorder=1)
			hold=True

		def mSTDP():
			clf()
			y=stdprule(x,-0.05,30,0.05,30)

			doPlot(x,y)
			plt.scatter(spot,stdprule(spot,-0.05,30,+0.05,30),marker='v',color='blue',zorder=2)
			plt.scatter(spot,stdprule(spot,-0.05,30,0.05,30),marker='+',color='red',zorder=2,s=30)

			plt.xlabel(r'hid-vis ($t_{l}-t_{k}$) $(ms)$',size=12)
			plt.ylabel('$\Delta w_{ij}\;,\;\eta/\zeta\Delta q_{ji} \; (mV)$',size=12)
			plt.title('mSTDP')
			self.add_file_to_save('mSTDP')
		def STDP():
			y=stdprule(x,-0.05,30,0.05,30)
			doPlot(x,y)
			plt.xlabel(r'post-pre ($t_{l}-t_{k}$) $(ms)$',size=12)
			plt.ylabel('$\Delta w_{ij} \; (mV)$',size=14)
			plt.title('STDP')
			plt.plot(x,y,color='black',zorder=1)
			hold=True
			plt.scatter(spot,stdprule(spot,-0.05,30,0.05,30),marker='+',color='red',zorder=2,s=30)
			plt.scatter(-spot-10,stdprule(-spot,-0.05,30,+0.05,30),marker='v',color='blue',zorder=2,s=30)
			self.add_file_to_save('STDP')
		def rSTDP():
			y=stdprule(x,0.05,30,-0.05,30)
			doPlot(x,y)
			plt.xlabel(r'post-pre ($t_{k}-t_{l}$) $(ms)$',size=12)
			plt.ylabel('$\eta/\zeta \Delta q_{ji} \; (mV)$',size=14)
			plt.title('rSTDP')
			hold = True
			plt.scatter(-spot-10,stdprule(-spot,0.05,30,-0.05,30),marker='^',color='blue',zorder=2)
			self.add_file_to_save('rSTDP')


		mSTDP()
		STDP()
		rSTDP()


	def biologicalNeuronsPlot(self,figsize=(4,4)):
		figure(figsize=figsize)
		def add_triangle(ax,xy,width=0.2,height=0.2,**kwargs):
			ax.add_patch(Polygon(((xy[0]-width/2,xy[1]),(xy[0],xy[1]+height),(xy[0]+width/2,xy[1])),**kwargs))

		def add_axon(ax,from_neuron,offsets_x,offsets_y,synapse_radius=0.02,**kwargs):
			xs=(from_neuron['xy'][0],)+tuple(np.add(from_neuron['xy'][0],offsets_x))
			ys=(from_neuron['xy'][1],)+tuple(np.add(from_neuron['xy'][1],offsets_y))
			ax.add_line(matplotlib.lines.Line2D(xs,ys,**kwargs))
			circle = matplotlib.patches.Circle((xs[-1],ys[-1]), radius=synapse_radius,**kwargs)
			ax.add_patch(circle)

		def add_dendrite(ax,neuron,neuron_height=0.2,height=0.25,width=0.35,**kwargs): 
			line1=matplotlib.lines.Line2D((neuron['xy'][0],neuron['xy'][0]),(neuron['xy'][1]+neuron_height,neuron['xy'][1]+neuron_height+height),**kwargs)
			ax.add_line(line1)
			line2=matplotlib.lines.Line2D((neuron['xy'][0],neuron['xy'][0]-width/2),(neuron['xy'][1]+neuron_height+height,neuron['xy'][1]+neuron_height+height*1.5),**kwargs)
			ax.add_line(line2)
			line3=matplotlib.lines.Line2D((neuron['xy'][0],neuron['xy'][0]+width/2),(neuron['xy'][1]+neuron_height+height,neuron['xy'][1]+neuron_height+height*1.5),**kwargs)
			ax.add_line(line3)
			line4=matplotlib.lines.Line2D((neuron['xy'][0]-width/2,neuron['xy'][0]+width/2),(neuron['xy'][1],neuron['xy'][1]),**kwargs)
			ax.add_line(line4)
			
		def add_label(neuron,thetext,displacement=(-0.5,0.1)):
			text(neuron['xy'][0]+displacement[0],neuron['xy'][1]+displacement[1],s=thetext)
		 #   text(np.add(neuron['xy'],(0.1,0.1)),text)
			
		clf()
		figure(1)
		ax=subplot(1,1,1,aspect='equal')
		ax.autoscale(enable=False)
		visible={'xy':(0.4,0.1),'color':'k'}
		hidden={'xy':(1,0.9),'color':'grey'}

		add_triangle(ax,alpha=0.5,**hidden)
		add_triangle(ax,alpha=0.5,**visible)
		add_label(visible,'Visible \nneuron')
		add_label(hidden,'Hidden \nneuron')

		add_dendrite(ax,visible,alpha=0.5,color=visible['color'])
		add_dendrite(ax,hidden,alpha=0.5,color=hidden['color'])
		add_axon(ax,visible,(0,.75,.75),(-.1,-.1,.795),color='r',linestyle='dashed')
		text(1.2,0.6,'Feedforward \n connection',color='r')
		text(0.48,0.35,'Feedback \n connection',color='b')

		add_axon(ax,hidden,(0,-.45),(-.25,-.25),color='b',linestyle='dashdot',linewidth=2)
		#ax.add_line(matplotlib.lines.Line2D((visible['xy'][0],visible['xy'][0],hidden['xy'][0]),(visible['xy'][1],visible['xy'][1]-.2,hidden['xy'][1]),lw=5., alpha=0.3))

		ax.set_xlim(-.1,2)
		ax.set_ylim(-0.05,1.6)
		plt.axis('off')

		self.add_file_to_save('neurons')
		#plt.show()

	def lossFunctionPlot(self,beta=3,figsize=(3.0,3)):
		assert self.an is not None
		figure(figsize=figsize)
		an=self.an
		afds=(np.mean(an.activation_deviations,axis=1))
		clf()
		hold = True
		plot(an.pns,(1-np.array(an.corrs))+beta*afds,'k')
		plot(an.pns,(1-np.array(an.corrs)),'k--')
		plot(an.pns,beta*afds,'k-.')
		xlabel('Number of training presentations')
		ylabel('Loss')
		legend(('Total loss','Reconstruction loss','Sparsity loss'))
		ylim(0,1)
		plt.gca().xaxis.set_major_locator( MaxNLocator(4))
		self.add_file_to_save(self.dataname+'_losses')
		#xlim(0,3000)

	def symmetryFunctionPlot(self,beta=3,figsize=(3.0,3)):
		assert self.an is not None
		figure(figsize=figsize)
		an=self.an
		afds=(np.array(an.ff_fb_corrs))
		clf()
		hold = True
		plot(an.pns,afds,'k')
		xlabel('Number of training presentations')
		ylabel(r'Correlation W, Q^T')
		ylim(0,1.1)
		plt.gca().set_xscale('log')
		#plt.gca().xaxis.set_major_locator( MaxNLocator(4))
		self.add_file_to_save(self.dataname+'_symmetry')
		#xlim(0,3000)


	def rasterPlot(self,figsize=(3.0,3),**kwargs):
		assert self.an is not None
		figure(figsize=figsize)
		self.an.net.P.rasterPlot(**kwargs)
		self.add_file_to_save(self.dataname+'_realRasterPlot')

	def receptiveFieldsPlot(self,figsize=(4,4)):
		assert self.an is not None
		figure(figsize=figsize)
		self.an.net.weights.showReceptiveFields()
		self.add_file_to_save(self.dataname+'_receptiveFields')

	def reconstructionAttemptsPlot(self,figsize=(8,3)):

		assert self.an is not None
		figure(figsize=figsize)
		
		self.an.plots.plotReconstructionAttempts(None,self.an.numericalPresentationInstance,presnumber=self.presnumber,factor=None)
		self.add_file_to_save(self.dataname+'_reconstructionAttempts')

	def reconstructionAttemptsScaledPlot(self,figsize=(8,3)):
		if self.factor:
			assert self.an is not None
			figure(figsize=figsize)
		
			self.an.plots.plotReconstructionAttempts(None,self.an.numericalPresentationInstance,presnumber=self.presnumber,factor=self.factor)
			self.add_file_to_save(self.dataname+'_reconstructionAttemptsScaled')


	def reconstructionReceptiveFieldsPlot(self,figsize=(6.5,3),**kwargs):
		figure(figsize=figsize)
		self.an.plots.plotReconstructionReceptiveFields(presNumber=self.presnumber,factor=None,**kwargs)
		self.add_file_to_save(self.dataname+'_reconstructionReceptiveFields')

	def reconstructionReceptiveFieldsScaledPlot(self,figsize=(6.5,3),**kwargs):
		if self.factor:
			figure(figsize=figsize)
			self.an.plots.plotReconstructionReceptiveFields(presNumber=self.presnumber,factor=self.factor,**kwargs)
			self.add_file_to_save(self.dataname+'_reconstructionReceptiveFieldsScaled')

	def historicalReconstructionAttemptsPlot(self,figsize=(6,3),**kwargs):
		figure(figsize=figsize)
		self.an.plots.plotHistoricalReconstructionAttempts(presnumber=self.presnumber,factor=None,pNs=self.pNs)
		self.add_file_to_save(self.dataname+'_historicalReconstructionAttempts')

	def historicalReconstructionAttemptsScaledPlot(self,figsize=(6,3),**kwargs):
		if self.factor:
			figure(figsize=figsize)
			self.an.plots.plotHistoricalReconstructionAttempts(presnumber=self.presnumber,factor=self.factor,pNs=self.pNs)
			self.add_file_to_save(self.dataname+'_historicalReconstructionAttemptsScaled')


	def weightHistoryPlot(self,figsize=(8,4)):
		figure(figsize=figsize)
		self.an.plots.plotWeightHistory()
		self.add_file_to_save(self.dataname+'_weightHistory')

	def weightCorrsPlot(self,figsize=(4,2)):
		figure(figsize=figsize)
		self.an.plotWeightCorrs()
		self.add_file_to_save(self.dataname+'_weightCorrs')

	def activityCorrsPlot(self,figsize=(4,2)):
		figure(figsize=figsize)
		self.an.plotActivityCorrs()
		self.add_file_to_save(self.dataname+'_activityCorrs')


