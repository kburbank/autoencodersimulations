import autoencoder
import unittest
import numpy as np

param_list = ['MNIST_Dataset','somethingelse']



class _DatasetsBaseTest(object):

	def setUp(self):
		self.config = autoencoder.aeconfig.Config(datatype=self.datasetname)
		self.dat=getattr(autoencoder.datasets,self.datasetname)(self.config)
		

	def test_dataset_max(self):
		assert np.amax(self.dat.nextdata())<=1

	def test_dataset_min(self):
		if self.dat.combine:
			# Combine is the variable that determines whether we want to stay non-negative
			assert np.amin(self.dat.nextdata())>=0
		else:
			assert np.amin(self.dat.nextdata())>=-1

	def test_different_stims_not_equal(self):
		dat1=self.dat.nextdata()
		dat2=self.dat.nextdata()
		assert any(dat1 != dat2)

	def test_dataset_size(self):
		dat1=self.dat.nextdata()
		assert len(dat1)==self.config.n_x

	def test_dataset_std_nonzero(self):
		assert np.std(self.dat.nextdata()>0)

	def test_dataset_ntrain_increments(self):
		n1=self.dat.n_train
		self.dat.nextdata()
		n2=self.dat.n_train
		assert n2==1+n1



class MNISTTest(_DatasetsBaseTest,unittest.TestCase):
	datasetname='MNIST_Dataset'

class MNISTNoCenterTest(_DatasetsBaseTest,unittest.TestCase):
	datasetname='MNIST_No_Center_Dataset'

class MNISTNoSubtractTest(_DatasetsBaseTest,unittest.TestCase):
	datasetname='MNIST_No_Subtract_Dataset'


class FilteredImagesTest(_DatasetsBaseTest,unittest.TestCase):
	datasetname='filtered_Images_Dataset'

class FilteredNoCenterTest(_DatasetsBaseTest,unittest.TestCase):
	datasetname='filtered_Images_No_Center_Dataset'
