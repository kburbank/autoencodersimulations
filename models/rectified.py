from autoencoder import network, simulation, save
from autoencoder.defaultmodels import genericTwoLayer
import numpy as np
from numpy import sum, mean, clip, ones, dot, corrcoef, zeros
from math import *
import tables
from pylab import  cla,scatter, hist




class RectifiedPlasticity(genericTwoLayer.Plasticity):

	def _calcDW(self, weights, P):
		if any(P.z1):
			dW = self.cv.learningRate * \
				np.outer(P.input_rates - P.x2, P.z1) - \
				self.cv.weight_regularization * weights.W
			dWf = dW
		else:
			dW = np.zeros((self.architecture.n_x, self.architecture.n_z)) - \
				self.cv.weight_regularization * weights.W
			dWf = 0

		return dW, dWf

class MultiplicativeWeights(genericTwoLayer.Weights):

	def calcWeff(self):
		""" The effective feedforward weights, including the effects of synaptic scaling (theta) and constant inhibition
		"""
		return self.W * self.theta - self.cv.const_inh
	def changeWeights(self, dW, dWf=[], dtheta=0):
		""" Update the weights and thetas, given desired change amoutns dW, dtheta etc, and do clipping.

		We have a separate function for this so that we can clip the weights to their max and min and keep
		track of how many weight updates there have been.
		"""
		if dWf == []:
			dWf = dW
		self.W += dW
		self.Wf += dWf
		self.W = np.clip(self.W, self.cv.min_W, self.cv.max_W)
		self.Wf = np.clip(self.Wf, self.cv.min_W, self.cv.max_W)
		self.theta += dtheta
		self.theta = np.clip(self.theta,self.cv.min_theta,self.cv.max_theta)
		self.dW = dW
		self.number_of_weight_updates += 1
		self.applyCalculatedVariables()

class RectifiedGradientPlasticity(genericTwoLayer.Plasticity):

	def _calcDW(self, weights, P):
		epsilon = P.input_rates - P.x2
		fprime = 1*(P.z1 >1)
		gprime = 1*(P.x2>1)
		dW = self.cv.learningRate * ( \
			gprime * np.outer(epsilon, P.z1) +
			 np.outer(P.input_rates, (dot(gprime*epsilon, weights.W) * fprime)))
		dWf = dW
		return dW, dWf

	def calcDTheta(self, weights, P):
		epsilon = P.input_rates - P.x2
		fprime = P.z1 * (1 - P.z1)

		return gprime*(self.cv.thetaLearningRate * dot(epsilon, weights.W)
				* fprime)

class MultiplicativeInhibitiveWeights(genericTwoLayer.WeightsPlussInhib, MultiplicativeWeights):
	def calcWeff(self):
		""" The effective feedforward weights, including the effects of synaptic scaling (theta) and constant inhibition
		"""
		return self.W * self.theta


	def calcWeffnonscale(self):
		""" The effective feedback weights, including the effects of  constant inhibition
		"""
		
		return self.Wf
		
class MultiplicativeInhibitiveScaledFeedbackWeights(MultiplicativeInhibitiveWeights):
	def calcWeffnonscale(self):
		""" The effective feedback weights, including the effects of  constant inhibition
		"""
		if self.cv.const_inh == 0:
			return self.Wf / self.avg_firing_rates
		else:
			return self.Wf / self.avg_firing_rates - self.cv.const_inh


class SparseRectifiedGradientPlasticity(genericTwoLayer.Plasticity):

	def _calcDW(self, weights, P):
		epsilon = P.input_rates - P.x2
		fprime = 1*(P.z1 >1)+.1*(P.z1<=0)
		gprime = 1*(P.x2>1)+.1*(P.x2<=0)
		

		rho = weights.avg_firing_rates
		rhohat = self.cv.pp
		rhofactor = (rhohat - rho)
		
		dW1 = self.cv.learningRate * ( \
			gprime[:,np.newaxis] * np.outer(epsilon, P.z1) +
			 np.outer(P.input_rates, (dot(gprime*epsilon, weights.W) * fprime)))
		dW2 = self.cv.thetaLearningRate * \
			(np.outer(P.input_rates, rhofactor))
		dW = dW1 + dW2 
		dWf = dW
		return dW, dWf


	def calcDTheta(self, weights, P):
		epsilon = P.input_rates - P.x2
		fprime = 1*(P.z1 >1)+.1*(P.z1<=0)
		gprime = 1*(P.x2>1)+.1*(P.x2<=0)

		rho = weights.avg_firing_rates
		rhohat = self.cv.pp
		#rhofactor = ((-rho / rhohat) + (1 - rho) / (1 - rhohat)) * fprime
		rhofactor = (rhohat - rho)
		return (self.cv.learningRate * dot(gprime*epsilon, weights.W) * fprime +
				self.cv.thetaLearningRate * rhofactor)






class RectifiedPresentation(genericTwoLayer.Presentation):


	def doPresentation(self, input_rates, weights):
		self.input_rates = input_rates
		self.z1 = (clip(dot(input_rates, weights.Weff), 0.0, 100000))
		self.x2 = clip(dot((self.z1), weights.Weffnonscale.T), 0.0, 100000)
#		self.x2 = clip(dot((self.z1), weights.Weffnonscale.T), 0.0, 100000)
		self.weight_update_number = weights.number_of_weight_updates
		if self.config.configObject.getint('plasticity','use_binary_activity_for_theta') == 0:
			self.frsum = (self.z1)
		else:
			self.frsum = (self.z1 > self.cv.z1_activity_threshold)

		self.presentation_number += 1

	def _applyConfigVariables(self, config):
		pass

	def rasterPlot(self):
		hist(self.z1, 100, hold=True)
		hist(self.x2, 100, hold=True)
		hist(self.input_rates, 100, hold=True)




class partiallyRectifiedPresentation(RectifiedPresentation):
	# When we don't rectify the visual inputs

	def doPresentation(self, input_rates, weights):
		self.input_rates = input_rates
		self.z1 = clip(dot(input_rates, weights.W) +
					   weights.theta * mean(abs(input_rates) * .01), 0.0, 100000)
		# We can't use weights.Weff here, because the synaptic scaling as defined there only works when the
		# average activation of the visible units is greater than zero...
		# here's it's likely about equal to zero!
		self.x2 = dot(self.z1, weights.Weffnonscale.T)
		self.weight_update_number = weights.number_of_weight_updates
		if self.cv.use_binary_activity_for_theta == 1:
			self.frsum = (self.z1)
		else:
			self.frsum = (self.z1 > 0)
		self.presentation_number += 1


class NoHomeostasisRectifiedWeights(genericTwoLayer.Weights):

	def calcWeff(self):
		""" The effective feedforward weights, including the effects of synaptic scaling (theta) and constant inhibition
		"""
		return self.W  + self.cv.const_theta 


	def initializeWeights(self):
		super(NoHomeostasisRectifiedWeights,self).initializeWeights()
		self.W += self.cv.initial_weight_shift

class Model(genericTwoLayer.Model):

	name = 'Rectified autoencoder model'

	plasticity = RectifiedPlasticity

	presentation = RectifiedPresentation

	paramstring = '''
	
[presentation]
z1_activity_threshold=0.0
	'''

class NonSymmetricWeightsModel(Model):

	name = 'Non symmetric initial weights'

	weights = genericTwoLayer.NonSymmetricWeights

class NoHomeostasisModel(Model):

	name = 'No homeostasis autoencoder'

	weights = NoHomeostasisRectifiedWeights

	paramstring = '''

[weights]

const_theta = -.02
initial_weight_shift = .05

'''



class AverageHomeostasisPlasticity(RectifiedPlasticity):
	def calcDTheta(self, weights, P):

		return self.cv.thetaLearningRate * \
			(self.cv.pp - np.ones(np.size(weights.avg_firing_rates))*np.mean(weights.avg_firing_rates))

class AverageHomeostasisModel(Model):

	name = 'Averaged Homeostasis autoencoder'

	plasticity = AverageHomeostasisPlasticity

class MultiplicativeScalingModel(Model):

	name = 'Multiplicative scaling model'

	weights = MultiplicativeWeights

	paramstring = '''
	
[presentation]
z1_activity_threshold=0.1
	'''


class PartiallyRectifiedModel(Model):

	name = 'Partially Rectified'

	presentation = partiallyRectifiedPresentation
