from autoencoder import network, simulation, save
from autoencoder.defaultmodels import genericTwoLayer

import numpy as np
from models import rectified
from scipy.ndimage.filters import gaussian_filter
from numpy import sum, mean, clip, ones, dot, corrcoef, zeros,shape
from math import *
from tables import IntCol, FloatCol, Float32Col, UInt16Col,UIntCol


class Weights(genericTwoLayer.Weights):
	def initializeWeights(self):
		super(Weights,self).initializeWeights()
		self.pps = np.random.lognormal(mean=np.log(self.cv.pp_mean),sigma=self.cv.pp_sigma,size=self.architecture.n_z)


class Plasticity(rectified.RectifiedPlasticity):
	def calcDTheta(self, weights, P):
		return self.cv.thetaLearningRate * \
			(weights.pps - weights.avg_firing_rates)


class Model(rectified.Model):

	name = 'Variable pp model'

	plasticity = Plasticity

	weights = Weights

	paramstring = '''
[weights]
pp_mean = 0.01
pp_sigma = 0.5

'''
