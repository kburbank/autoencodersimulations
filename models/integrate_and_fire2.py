from autoencoder import network, simulation, save
from autoencoder.models import genericTwoLayer
import numpy as np
from numpy import sum, mean, clip, ones, dot, corrcoef, zeros
from math import *
import tables
import scipy
from pylab import figure, clf, subplot, cla, hist, gcf, scatter
from autoencoder.aeconfig import configView




class IntegrateAndFireSimulation(simulation.Simulation):

# Just for convenience!
	def __init__(self,**kwargs):
		super(IntegrateAndFireSimulation, self).__init__(ModelParams(),**kwargs) 


	def add_savelist(self):
		self.savelist = np.unique(
				np.hstack((np.round(np.logspace(0, 6, num=200)), self.cv.max_presentations)))




class integrateAndFirePlasticity(genericTwoLayer.Plasticity):

	LTD_calculated = False

	def calculate_LTD_LTP(self,sample_presentation):
		self.cv=configView(self.config.configObject,sectionname='plasticity')
		self.cvi=configView(self.config.configObject,sectionname='plasticity',function_name='getint')

		run_time = sample_presentation.cv.run_time
		step_size = sample_presentation.cv.step_size

		self.iandfcv=configView(self.config.configObject,sectionname='presentation')

		self.timediff = np.arange(-run_time,run_time, step_size)
		self.LTP = np.exp(-(clip(self.timediff, 0, 3000 /
								 step_size) / (self.cv.tau_LTP)))
		self.LTD = np.exp((clip(self.timediff, -3000 / step_size,
								0) / (self.cv.tau_LTD))) * self.cv.depression_bias
		self.LTP[self.LTP == 1] = 0
		self.LTD[self.LTD == self.cv.depression_bias] = 0
		self.offset = int(run_time / step_size)
		self.dW = np.zeros((self.architecture.n_z, self.architecture.n_x))
		self.dWf = self.dW.copy()
		self.__class__.LTD_calculated = True

	def _dWfromList(self, snx, snz):

		indices_z = (snz[1])
		times_z = snz[0]
		indices_x = snx[1]
		times_x = snx[0]
		timesxx, timeszz = np.meshgrid(times_x, times_z)
		indicesxx, indiceszz = np.meshgrid(indices_x, indices_z)
		di = timeszz - timesxx + self.offset
		ch = self.LTP[di] - self.LTD[di]
		return scipy.sparse.coo_matrix(
			(ch.reshape(-1), (indicesxx.reshape(-1), indiceszz.reshape(-1))), shape=(self.architecture.n_x, self.architecture.n_z)).todense()

	def _calcDW(self, weights, P):

		if self.__class__.LTD_calculated is False:
			self.calculate_LTD_LTP(P)
			assert self.__class__.LTD_calculated is True

		snz = (P.spike_neurons_z.nonzero())
		snx = (P.spike_neurons_x.nonzero())

		x_chunks = self.chunks(snx, 1500)
		z_chunks = self.chunks(snz, 1500)
		dW = np.zeros((self.architecture.n_x, self.architecture.n_z))

		for x_chunk in x_chunks:
			for z_chunk in z_chunks:
				dW += self._dWfromList(x_chunk, z_chunk)
		return self.cv.learningRate * dW, self.cv.learningRate * dW



class integrateAndFirePresentation(genericTwoLayer.Presentation):
	append_dict = {'input_rates':'input_rates','z1':'z1','x2':'x2','pN':'weight_update_number'
#	,'spike_neurons_x':'spike_neurons_x','spike_neurons_z':'spike_neurons_z'
	}


	def setupFields(self):
		self._initializeState()
		self.presentation_number = 0
		self.weight_update_number = 0

		self.presentation_table = {
			"input_rates": tables.FloatCol(shape=(self.architecture.n_x)),
			"z1": tables.FloatCol(shape=self.architecture.n_z),
			"x2": tables.FloatCol(shape=self.architecture.n_x),
			"pN": tables.IntCol(),
	#		"spike_neurons_x" : tables.IntCol(shape=(self.cv.run_time/self.cv.step_size,self.architecture.n_x)),
	#		"spike_neurons_z" : tables.IntCol(shape=(self.cv.run_time/ self.cv.step_size,self.architecture.n_z))
		} 

		self.x2cacheIsCurrent = False
		self.z1cacheIsCurrent = False


	def applyCalculatedVariables(self):

		if self.__class__.calculated_variables_created is False:

			self.__class__.cv=configView(self.config.configObject,sectionname='presentation')
			self.__class__.cvi=configView(self.config.configObject,sectionname='presentation',function_name='getint')
			self.__class__.threshold_z =  self.cv.threshold_z
		#       self.threshold_z[0:self.number_highly_excitable]=self.highly_excitable_threshold

			self.__class__.number_steps = int(self.cvi.run_time / self.cv.step_size)
			self.__class__.time_decay = exp(-(self.cv.step_size / self.cv.tau_decay))
			self.__class__.adap_time_decay = exp(
				-(self.cv.step_size / self.cv.tau_adap_decay))
			self.stopInput = int(
				self.cv.stimulus_on_duration / self.cv.step_size)

			def alpha(t, tau):
				return (t / tau * np.exp(1 - t / tau))
			self.__class__.alphaProfile = alpha(np.arange(
				0, self.cv.epsp_width / self.cv.step_size ), self.cv.synaptic_tau)
			self.__class__.alphaProfile /= sum(self.alphaProfile)
			self.__class__.alphaMatrix = np.tile(
				self.alphaProfile, (max(self.architecture.n_x, self.architecture.n_z), 1))
			self.__class__.alphaMatrix = self.alphaMatrix.T
			self.__class__.alphaProfileIpsp = alpha(np.arange(
				0, self.cv.ipsp_width), self.cv.synaptic_tau)
			self.__class__.alphaProfileIpsp /= sum(self.alphaProfileIpsp)
			self.__class__.calculated_variables_created = True
		assert self.__class__.calculated_variables_created == True
		self.input_rates = zeros(self.architecture.n_x)


	# We calculate the total number of spikes for each neuron and save that when we save presentations,
	# rather than the whole timecourse.  But then when we recreate a presentation from saved values,
	# we need to note that we've populated the summed fields (x2 and z1) and not to try to recalculate these
	# from the now-blank spike_neurons_z and spike_neurons_x fields!
	def get_z1(self):
		if self.z1cacheIsCurrent:
			return self.z1cache
		else:
			self.z1cache = np.sum(self.spike_neurons_z, axis=0)
			self.z1cacheIsCurrent = True
			return self.z1cache

	def set_z1(self,val):
		self.z1cache = val
		self.z1cacheIsCurrent = True

	z1 = property(get_z1,set_z1)

	def get_x2(self):
		if self.x2cacheIsCurrent:
			return self.x2cache
		else:
			self.x2cache = np.sum(self.spike_neurons_x[self.stopInput:, :], axis=0)
			self.x2cacheIsCurrent = True
			return self.x2cache

	def set_x2(self,val):
		self.x2cache = val
		self.x2cacheIsCurrent = True

	x2 = property(get_x2,set_x2)


	def updateAvgFiringRates(self):

		if self.cvi.use_spike_count_for_theta == 1:
			self.frsum = clip(sum(self.spike_neurons_z, axis=0), 0, 1000)
		else:
			self.frsum = clip(sum(self.spike_neurons_z, axis=0), 0, 1)

	def _initializeState(self):
		self.znz = np.zeros(self.architecture.n_z)
		self.xnz = np.zeros(self.architecture.n_x)
		self.ge_x = np.zeros(self.architecture.n_x)
		self.ge_z = np.zeros((self.architecture.n_z))
		self.x2cache = []
		self.z1cache = []
		if self.cv.adap_rise_amount_x:
			self.adap_x = np.zeros(self.architecture.n_x)
		else:
			self.adap_x = 0
		if self.cv.adap_rise_amount_z:
			self.adap_z = np.zeros(self.architecture.n_z)
		else:
			self.adap_z = 0
		self.change_arrays_x = np.zeros([self.number_steps, self.architecture.n_x])
		self.change_arrays_z = np.zeros([self.number_steps, self.architecture.n_z])
		self.spike_neurons_x = np.zeros(
			[self.number_steps, self.architecture.n_x], dtype='int8')
		self.spike_neurons_z = np.zeros(
			[self.number_steps, self.architecture.n_z], dtype='int8')

		if self.cv.x_noise_level > 0:
			self.myRandsX = np.zeros((self.number_steps, self.architecture.n_x))

		if self.cv.z_noise_level > 0:
			self.myRandsZ = np.zeros((self.number_steps, self.architecture.n_z))

	def resetState(self):
		self.ge_x *= 0
		self.ge_z *= 0
		self.neurons_fired_x = []
		self.neurons_fired_z = []
		self.adap_x *= 0
		self.adap_z *= 0
		self.znz = []
		self.xnz = []
		self.change_arrays_x *= 0
		self.change_arrays_z *= 0
		self.spike_neurons_x *= 0
		self.spike_neurons_z *= 0
		self.x2cacheIsCurrent=False
		self.z1cacheIsCurrent=False
		if self.cv.x_noise_level > 0:
			self.myRandsX *= 0
			self.myRandsX += np.random.normal(size=(self.number_steps, self.architecture.n_x),
											  scale=self.cv.x_noise_level, loc=self.cv.x_noise_loc)
		if self.cv.z_noise_level > 0:
			self.myRandsZ *= 0
			self.myRandsZ += np.random.normal(size=(self.number_steps, self.architecture.n_z),
											  scale=self.cv.z_noise_level, loc=self.cv.z_noise_loc)

	def doPresentation(self, input_rates, weights):
		thesum = 0
		self.weight_update_number = weights.number_of_weight_updates

	# re-initialize the network
		self.resetState()
		# Now set up the data we are using for the problem.
		input_rates = self.cv.step_size * \
			input_rates * self.cv.input_rate_scale

		input_rates_save = input_rates.copy()

		for step_number in xrange(0, self.number_steps):
			if self.cv.z_noise_level > 0:
				randz=self.myRandsZ[step_number,:]
			else:
				randz=0
			if self.cv.x_noise_level > 0:
				randx=self.myRandsX[step_number,:]
			else:
				randx=0
			if step_number == self.stopInput:
				input_rates *= 0
				# print thesum
				if self.cv.reset_membrane_potential:
					self.ge_x *= 0


			if step_number >= self.cv.delay/self.cv.step_size:

				self.ge_x = ((self.ge_x+self.change_arrays_x[(step_number-self.cv.delay/self.cv.step_size), :]+input_rates - self.adap_x+randx )*self.time_decay)
				self.ge_x = self.ge_x.clip(self.cv.lower_bound, 21)
				self.ge_z = ( (self.ge_z+self.change_arrays_z[(step_number-self.cv.delay/self.cv.step_size), :]-self.adap_z+randz )*self.time_decay).clip(self.cv.lower_bound, self.threshold_z+1)
				self.adap_z *= self.adap_time_decay
				self.neurons_fired_z = self.ge_z > self.threshold_z
				self.znz = self.neurons_fired_z.nonzero()[0]
			else:
				self.ge_x = ((self.ge_x+input_rates - self.adap_x+randx )*self.time_decay).clip(self.cv.lower_bound, 21)
			self.adap_x *= self.adap_time_decay

		#  figure out which neurons fired
			self.neurons_fired_x = self.ge_x > 20
			self.xnz = self.neurons_fired_x.nonzero()[0]
			apm = self.alphaMatrix[
				0:min(self.cv.epsp_width/self.cv.step_size, self.number_steps - step_number), 0:self.architecture.n_z]
			apmx = self.alphaMatrix[
				0:min(self.cv.epsp_width/self.cv.step_size, self.number_steps - step_number), 0:self.architecture.n_x]

			mySlice = (
				slice(step_number, step_number + self.cv.epsp_width/self.cv.step_size), slice(None))

			# This is the part of the change arrays we will add to this
			# timestep.
			if any(self.xnz):
				#       if 1:
				if self.cv.use_standard_bias:
					#                   np.add(self.change_arrays_z[mySlice],np.outer(ap,self.cv.const_excit+weights.theta+np.sum(weights.Weffnonscale.T[:,self.xnz],axis=1)),out=self.change_arrays_z[mySlice])
					#                   np.add(self.change_arrays_z[mySlice],out,axis=1)),out=self.change_arrays_z[mySlice])
					#                   np.add(self.change_arrays_z[mySlice],np.outer(ap,self.cv.const_excit+weights.theta+np.dot(weights.Weffnonscale.T,self.neurons_fired_x)),out=self.change_arrays_z[mySlice])
					ff = np.tile(self.cv.const_excit + weights.theta + np.sum(weights.Weffnonscale.T[
								 :, self.xnz], axis=1).T, (min(self.cv.epsp_width/self.cv.step_size, self.number_steps - step_number), 1))

					thesum += 1
				else:
					ff = np.tile(self.cv.const_excit + weights.theta * len(self.xnz) + np.sum(weights.Weffnonscale.T[
								 :, self.xnz], axis=1).T, (min(self.cv.epsp_width/self.cv.step_size, self.number_steps - step_number), 1))

#                   np.add(self.change_arrays_z[mySlice],np.outer(ap,self.cv.const_excit+weights.theta*len(self.xnz)+np.sum(weights.Weffnonscale.T[:,self.xnz],axis=1)),out=self.change_arrays_z[mySlice])
#                   np.add(self.change_arrays_z[mySlice],np.outer(ap,self.cv.const_excit+weights.theta*len(self.xnz)+np.dot(weights.Weffnonscale.T,self.neurons_fired_x)),out=self.change_arrays_z[mySlice])

					# thesum+=len(self.xnz)
				out1 = (ff * apm)
				# out2=np.outer(ap,self.cv.const_excit+weights.theta+np.sum(weights.Weffnonscale.T[:,self.xnz],axis=1))
				# pdb.settrace()
				np.add(
					self.change_arrays_z[mySlice], out1, out=self.change_arrays_z[mySlice])
#               self.change_arrays_z[mySlice]+=out
				if self.cv.adap_rise_amount_x:
					self.adap_x += self.cv.adap_rise_amount_x * \
						self.neurons_fired_x
				self.ge_x[self.xnz] = 0
				self.spike_neurons_x[step_number, :] = self.neurons_fired_x
			if any(self.znz):
				self.adap_z += self.cv.adap_rise_amount_z * \
					self.neurons_fired_z
				fb = np.tile(self.cv.const_excit + np.sum(weights.Weffnonscale[
							 :, self.znz], axis=1).T, (min(self.cv.epsp_width/self.cv.step_size, self.number_steps - step_number), 1))
#               out1=np.outer(ap,np.sum(weights.Weffnonscale[:,self.znz],axis=1))*self.cv.feedback_fraction
				out2 = fb * apmx * self.cv.feedback_fraction

				np.add(
					self.change_arrays_x[mySlice], out2, out=self.change_arrays_x[mySlice])
				self.ge_z[self.znz] = 0
				self.spike_neurons_z[step_number, :] = self.neurons_fired_z

		self.input_rates = input_rates_save
		self.updateAvgFiringRates()
		self.presentation_number += 1

	def rasterPlot(self):
		cla()
		scatter(self.spike_neurons_z.nonzero()[
				0], self.spike_neurons_z.nonzero()[1], s=1, color='red')
		scatter(self.spike_neurons_x.nonzero()[
				0], self.spike_neurons_x.nonzero()[1] + self.architecture.n_z, s=1)


class Model(genericTwoLayer.Model):

	name = 'Integrate and fire autoencoder model'

	plasticity = integrateAndFirePlasticity

	presentation = integrateAndFirePresentation



	paramstring = '''

[simulation]
runtype = iandf

[architecture]
n_z = 120

[presentation]
delay = 17 ; delay before the beginning of an EPSP, in ms
inh_delay = 10	  ; delay before the beginning of an IPSP, in ms

epsp_width = 30 ; width of EPSP (currently it's  a square function)
ipsp_width = 30  ; width of IPSP (currently it's  a square function)
synaptic_tau = 2

const_excit=0


reset_membrane_potential=0


stimulus_on_duration=20 ; duration of the stimulus presentaiton, in ms (starts at time=0)


input_rate_scale = 15 ;  amount by which to multiply input pixel values 
#(after normalizing by std?)

tau_decay = 30 ; time for decay of membrane potential, in ms
tau_adap_decay = 200 ; time for decay of  spike rate adaptation, in ms
adap_rise_amount_x = 0.0 ; rise amount in adaptation variable per ms
adap_rise_amount_z=0 ; rise amount in adaptation variable per ms


feedback_fraction= 0.25 ; the amount by which FB connections are scaled 
# relative to FF ones
# excitation for the same time difference betwen spikes
inh_level=0


x_noise_level=.0
z_noise_level=0
x_noise_loc=-0
z_noise_loc=.1

use_spike_count_for_theta = 0

x_bias=0.0
bias_learning_rate_factor=1

threshold_z = 20

lower_bound = -20

run_time=100 ; presentation running time, in ms
step_size=0.1 ; step size, in ms

use_standard_bias=0

[plasticity]

tau_LTP = 30 ; time constant for STDP, in ms
tau_LTD = 30 ; time constant for STDP, in ms
max_dw = 30
weight_change_probability=1
depression_bias = 4 ; the amount by which depression is stronger than 
learningRate=.01
thetaLearningRate=0.1

[weights]
initial_theta_mean=-0.01
initial_weight_scale=0.25

		'''

