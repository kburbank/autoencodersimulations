from autoencoder import network, simulation, save
import numpy as np
from autoencoder.defaultmodels import genericTwoLayer
from numpy import sum, mean, clip, ones, dot, corrcoef, zeros
from math import *
from tables import IntCol, FloatCol, Float32Col, UInt16Col,UIntCol



class SigmoidPresentation(genericTwoLayer.Presentation):

	def sigm(self, x):
		return 1 / (1 + np.exp(-x))

	# When we don't rectify the visual inputs
	def doPresentation(self, input_rates, weights):
		self.input_rates = input_rates
		self.z1 = self.sigm(dot(input_rates, weights.W) + weights.theta)
		# We can't use weights.Weff here, because the synaptic scaling as defined there only works when the
		# average activation of the visible units is greater than zero... here's it's likely about equal to zero!
		# xinput=self.sigm(dot(self.z1,weights.Weffnonscale.T))
		self.x2 = dot(weights.Weffnonscale, self.z1) + \
			weights.thetaVisible  # what about x biases?
		self.weight_update_number = weights.number_of_weight_updates
		if self.config.configObject.getint('plasticity','use_binary_activity_for_theta') == 1:
			self.frsum = (self.z1)
		else:
			self.frsum = (self.z1 > 0)
		self.presentation_number += 1





class sigmoidGradientPlasticity(genericTwoLayer.Plasticity):

	def _calcDW(self, weights, P):
		epsilon = P.input_rates - P.x2
		sprime = P.z1 * (1 - P.z1)
		dW = self.cv.learningRate * \
			(np.outer(epsilon, P.z1) +
			 np.outer(P.input_rates, (dot(epsilon, weights.W) * sprime)))
		dWf = dW
		return dW, dWf

	def calcDTheta(self, weights, P):
		epsilon = P.input_rates - P.x2
		sprime = P.z1 * (1 - P.z1)

		return (self.cv.thetaLearningRate * dot(epsilon, weights.W)
				* sprime, self.cv.thetaLearningRate * epsilon)


class sparseSigmoidPlasticity(genericTwoLayer.Plasticity):

	def _calcDW(self, weights, P):
		epsilon = P.input_rates - P.x2
		sprime = P.z1 * (1 - P.z1)
		rho = weights.avg_firing_rates
		rhohat = self.cv.pp
		rhofactor = ((-rho / rhohat) + (1 - rho) / (1 - rhohat)) * sprime
		dW1 = self.cv.learningRate * \
			(np.outer(epsilon, P.z1) +
			 np.outer(P.input_rates, (dot(epsilon, weights.W) * sprime)))
		dW2 = self.cv.thetaLearningRate * \
			(np.outer(P.input_rates, rhofactor))
		dW = dW1 + dW2 - self.cv.weight_regularization * weights.W
		self.cv.thetaLearningRate * \
			(self.cv.pp - weights.avg_firing_rates)
		dWf = dW
		return dW, dWf

	def calcDTheta(self, weights, P):
		epsilon = P.input_rates - P.x2
		sprime = P.z1 * (1 - P.z1)
		rho = weights.avg_firing_rates
		rhohat = self.cv.pp
		rhofactor = ((-rho / rhohat) + (1 - rho) / (1 - rhohat)) * sprime

		return (self.cv.learningRate * dot(epsilon, weights.W) * sprime +
				self.cv.thetaLearningRate * rhofactor, self.cv.learningRate * epsilon)

class WeightsPlusVisibleBias(genericTwoLayer.Weights):

	append_dict = {'pN':'number_of_weight_updates','W':'Wflat','Wf':'Wfflat',
	'avg_firing_rates':'avg_firing_rates','theta':'theta','thetaVisible':'thetaVisible'}


	""" Weights class that includes

	"""

	def __init__(self, config, **kwargs):
		super(WeightsPlusVisibleBias, self).__init__(config,**kwargs)
		self.thetaVisible = np.zeros(self.architecture.n_x)

	def changeWeights(self, dW, dWf=[], dtheta=(0, 0)):
		if dWf == []:
			dWf = dW
		self.W += dW
		self.Wf += dWf
		self.W = np.clip(self.W, self.cv.min_W, self.cv.max_W)
		self.Wf = np.clip(self.Wf, self.cv.min_W, self.cv.max_W)
		self.theta += dtheta[0]
		self.thetaVisible += dtheta[1]
		self.dW = dW
		self.number_of_weight_updates += 1

	def histThetaVisible(self):
		""" Plot a histogram of synaptic scaling values (thetas) """
		pylab.hist(self.thetaVisible, 100)
		pylab.title('Synaptic offsets for visible units')

	@property
	def weights_table(self):
		weights_table = {
			"pN": UIntCol(),
			"W": Float32Col(shape=(self.architecture.n_x * self.architecture.n_z)),
			"Wf": Float32Col(shape=(self.architecture.n_x * self.architecture.n_z)),
			"avg_firing_rates": FloatCol(shape=(self.architecture.n_z)),
			"theta": Float32Col(shape=(self.architecture.n_z)),
			"thetaVisible": Float32Col(shape=(self.architecture.n_x))
		}
		return weights_table


class Model(genericTwoLayer.Model):

	name = 'Sigmoid autoencoder model'

	plasticity = sigmoidGradientPlasticity

	presentation = SigmoidPresentation


	weights = WeightsPlusVisibleBias

class SparseModel(Model):

	name = 'Sparse sigmoid autoencoder model'

	plasticity = sparseSigmoidPlasticity

