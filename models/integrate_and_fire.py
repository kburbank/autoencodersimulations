from autoencoder import network, simulation, save
from autoencoder.defaultmodels import genericTwoLayer
import numpy as np
from numpy import sum, mean, clip, ones, dot, corrcoef, zeros
from math import *
import tables
import scipy
from pylab import figure, clf, subplot, cla, hist, gcf, scatter,xlabel,gca
import pylab
from autoencoder.aeconfig import configView
import copy
from matplotlib.ticker import FuncFormatter
from pdb import set_trace
from copy import copy




class IntegrateAndFireSimulation(simulation.Simulation):

# Just for convenience!
	def __init__(self,**kwargs):
		super(IntegrateAndFireSimulation, self).__init__(ModelParams(),**kwargs) 
		


	def add_savelist(self):
		self.savelist = np.unique(
				np.hstack((np.round(np.logspace(0, 6, num=200)), self.cv.max_presentations)))


class smartThetaPlasticity(genericTwoLayer.Plasticity):
	def calcDTheta(self, weights, P):
#		return self.cv.thetaLearningRate * clip(weights.avg_firing_rates,0.01,10) * \
#			(self.cv.pp - weights.avg_firing_rates) / \
#			(weights.avg_firing_rates*np.clip((1-weights.avg_firing_rates/self.cv.max_firing_rate),0.1,1)+.001)
#		return self.cv.thetaLearningRate * clip(weights.avg_firing_rates,0.01,10000) * \
#			(self.cv.pp - weights.avg_firing_rates) * 10

		return self.cv.thetaLearningRate * (self.cv.pp - weights.avg_firing_rates)
#		return self.cv.thetaLearningRate * np.sign(self.cv.pp-weights.avg_firing_rates)


class integrateAndFirePlasticity(smartThetaPlasticity):

	calculated_variables_created = False

	def calculate_LTD_LTP(self,sample_presentation):
		

		run_time = sample_presentation.cv.run_time
		step_size = sample_presentation.cv.step_size

		self.iandfcv=configView(self.config.configObject,sectionname='presentation')

		self.timediff = np.arange(-run_time,run_time, step_size)
		self.LTP = np.exp(-(clip(self.timediff, 0, run_time) / (self.cv.tau_LTP)))
		self.LTD = np.exp((clip(self.timediff, -run_time,
								0) / (self.cv.tau_LTD))) * self.cv.depression_bias
		self.LTP[self.LTP == 1] = 0
		self.LTD[self.LTD == self.cv.depression_bias] = 0
		self.offset = int(run_time / step_size)
		self.dW = np.zeros((self.architecture.n_z, self.architecture.n_x))
		self.dWf = self.dW.copy()
		self.calculated_variables_created = True

	def _dWfromList(self, snx, snz):

		indices_z = (snz[1])
		times_z = snz[0]
		indices_x = snx[1]
		times_x = snx[0]
		timesxx, timeszz = np.meshgrid(times_x, times_z)
		indicesxx, indiceszz = np.meshgrid(indices_x, indices_z)
		di = timeszz - timesxx + self.offset
		ch = self.LTP[di] - self.LTD[di]
		return scipy.sparse.coo_matrix(
			(ch.reshape(-1), (indicesxx.reshape(-1), indiceszz.reshape(-1))), shape=(self.architecture.n_x, self.architecture.n_z)).todense()

	def _calcDW(self, weights, P):

		if self.calculated_variables_created is False:
			self.calculate_LTD_LTP(P)
			assert self.calculated_variables_created is True

		snz = (P.spike_neurons_z.nonzero())
		snx = (P.spike_neurons_x.nonzero())

		x_chunks = self.chunks(snx, 1500)
		z_chunks = self.chunks(snz, 1500)
		dW = np.zeros((self.architecture.n_x, self.architecture.n_z))

		for x_chunk in x_chunks:
			for z_chunk in z_chunks:
				dW += self._dWfromList(x_chunk, z_chunk)
		if self.cv.weight_regularization > 0:
			dW-= self.cv.weight_regularization * weights.W
		return self.cv.learningRate * dW, self.cv.learningRate * dW

class PlasticityExcludingBursts(integrateAndFirePlasticity):

	def _calcDW(self,weights,P):
		if sum(P.z1)>self.cv.exclusion_threshold:
			if self.cv.weight_regularization > 0:
				dW= self.cv.learningRate*self.cv.weight_regularization * weights.W
			else:
				dW=0*weights.W
		else:
			dW,_=super(PlasticityExcludingBursts,self)._calcDW(weights,P)
		return dW,dW



class integrateAndFirePresentation(genericTwoLayer.Presentation):
	append_dict = {'input_rates':'input_rates','z1':'z1','x2':'x2','pN':'weight_update_number'
#	,'spike_neurons_x':'spike_neurons_x','spike_neurons_z':'spike_neurons_z'
	}


	def setupFields(self):
		self._initializeState()
		self.presentation_number = 0
		self.weight_update_number = 0

		self.presentation_table = {
			"input_rates": tables.FloatCol(shape=(self.architecture.n_x)),
			"z1": tables.UInt16Col(shape=self.architecture.n_z),
			"x2": tables.UInt16Col(shape=self.architecture.n_x),
			"pN": tables.UIntCol(),
	#		"spike_neurons_x" : tables.IntCol(shape=(self.cv.run_time/self.cv.step_size,self.architecture.n_x)),
	#		"spike_neurons_z" : tables.IntCol(shape=(self.cv.run_time/ self.cv.step_size,self.architecture.n_z))
		} 

		self.x2cacheIsCurrent = False
		self.z1cacheIsCurrent = False


	def applyCalculatedVariables(self):

		if self.calculated_variables_created is False:

			self.cv=configView(self.config.configObject,sectionname='presentation')
			self.cvi=configView(self.config.configObject,sectionname='presentation',function_name='getint')
			self.spike_threshold =  self.cv.spike_threshold
		#       self.spike_threshold[0:self.number_highly_excitable]=self.highly_excitable_threshold

			self.number_steps = int(self.cvi.run_time / self.cv.step_size)
			self.time_decay = exp(-(self.cv.step_size / self.cv.tau_decay))

			self.tau_decay = self.cv.tau_decay/self.cv.step_size
			self.current_multiplier = -expm1(-self.cv.step_size/self.cv.tau_current_decay)
			self.adap_x_multiplier = self.cv.adap_rise_amount_x*self.cv.step_size/self.cv.tau_adap_decay
			self.adap_z_multiplier = self.cv.adap_rise_amount_z*self.cv.step_size/self.cv.tau_adap_decay

			self.excitatory_reversal_potential=self.cv.excitatory_reversal_potential
			self.inhibitory_reversal_potential=self.cv.inhibitory_reversal_potential
			self.leak_potential=self.cv.leak_potential
			self.reset_potential=self.cv.reset_potential
			self.rmie=self.cv.rmie

			self.current_decay = exp(-self.cv.step_size/self.cv.tau_current_decay)
			self.adap_time_decay = exp(
				-(self.cv.step_size / self.cv.tau_adap_decay))
			self.stopInput = int(
				(self.cv.stimulus_start_time+self.cv.stimulus_on_duration) / self.cv.step_size)
			self.startInput = int(
				self.cv.stimulus_start_time / self.cv.step_size)


			self.calculated_variables_created = True
		assert self.calculated_variables_created == True
		self.input_rates = zeros(self.architecture.n_x)


	# We calculate the total number of spikes for each neuron and save that when we save presentations,
	# rather than the whole timecourse.  But then when we recreate a presentation from saved values,
	# we need to note that we've populated the summed fields (x2 and z1) and not to try to recalculate these
	# from the now-blank spike_neurons_z and spike_neurons_x fields!
	def get_z1(self):
		if self.z1cacheIsCurrent:
			return self.z1cache
		else:
			self.z1cache = self.spike_neurons_z.sum(axis=0)
			self.z1cacheIsCurrent = True
			return self.z1cache

	def set_z1(self,val):
		self.z1cache = val
		self.z1cacheIsCurrent = True

	z1 = property(get_z1,set_z1)

	def get_x2(self):
		if self.x2cacheIsCurrent:
			return self.x2cache
		else:

			self.x2cache = self.spike_neurons_x[self.stopInput:, :].sum(axis=0)
			self.x2cacheIsCurrent = True
			return self.x2cache

	def get_reconstruction(self,input_rates,weights,factor=None):

		if factor:
			weights.theta*=factor		
			weights.applyCalculatedVariables()
		x_reconstruction=input_rates*0
		for i in range(0,10):
			self.doPresentation(input_rates, weights, use_feedback=False)
			x_reconstruction += np.dot(self.z1,weights.Wf.T).clip(0,10000)
		x_reconstruction/=10
		#x_reconstruction = np.dot(np.dot(input_rates,weights.Weff),weights.Wf.T).clip(0,100)
		if factor:
			weights.theta/=factor
			weights.applyCalculatedVariables()
#		x_without_feedback = self.spike_neurons_x.sum(axis=0).copy()
#		self.doPresentation(input_rates, weights, use_feedback=True)
#		x_with_feedback = self.spike_neurons_x.sum(axis=0).copy()
#		return x_with_feedback-x_without_feedback
		return x_reconstruction

	def set_x2(self,val):
		self.x2cache = val
		self.x2cacheIsCurrent = True

	x2 = property(get_x2,set_x2)
	
	def dorands(self):
				return 0,0		# this means noise doesn't work at all


	def updateAvgFiringRates(self):

		if self.cvi.use_spike_count_for_theta == 1:
			self.frsum = clip(sum(self.spike_neurons_z, axis=0), 0, 1000)
		else:
			self.frsum = clip(sum(self.spike_neurons_z, axis=0), 0, 1)

	def _initializeState(self):
		self.znz = np.zeros(self.architecture.n_z)
		self.xnz = np.zeros(self.architecture.n_x)
		self.mnz = np.zeros((self.architecture.n_m))
		self.mxnz = np.zeros((self.architecture.n_mx))

		self.ge_x = np.ones(self.architecture.n_x)*self.leak_potential
		self.ge_z = np.ones((self.architecture.n_z))*self.leak_potential
		self.ge_m = np.ones(self.architecture.n_m)*self.leak_potential
		self.ge_mx = np.ones(self.architecture.n_mx)*self.leak_potential

		self.x2cache = []
		self.z1cache = []
		if self.cv.adap_rise_amount_x:
			self.adap_x = np.zeros(self.architecture.n_x)
		else:
			self.adap_x = 0
		if self.cv.adap_rise_amount_z:
			self.adap_z = np.zeros(self.architecture.n_z)
		else:
			self.adap_z = 0


		self.open_probabilities_x = np.zeros(self.architecture.n_x)
		self.open_probabilities_z = np.zeros(self.architecture.n_z)
		self.open_probabilities_m = np.zeros(self.architecture.n_m)
		self.open_probabilities_mx = np.zeros(self.architecture.n_mx)

		self.open_probabilities_x_inh = np.zeros(self.architecture.n_x)
		self.open_probabilities_z_inh = np.zeros(self.architecture.n_z)


		self.last_spikes_x = [[] for _dummy in xrange(int(self.cv.delay/self.cv.step_size))]
		self.last_spikes_z = [[] for _dummy in xrange(int(self.cv.delay/self.cv.step_size))]
		self.last_spikes_m = [[] for _dummy in xrange(int(self.cv.delay/self.cv.step_size))]
		self.last_spikes_mx = [[] for _dummy in xrange(int(self.cv.delay/self.cv.step_size))]


		self.spike_neurons_x = np.zeros(
			[self.number_steps, self.architecture.n_x], dtype='int8')
		self.spike_neurons_z = np.zeros(
			[self.number_steps, self.architecture.n_z], dtype='int8')
		self.spike_neurons_m = np.zeros([self.number_steps,self.architecture.n_m], dtype='int8')
		self.spike_neurons_mx = np.zeros([self.number_steps,self.architecture.n_mx], dtype='int8')

#		if (self.cv.x_noise_level > 0) or (self.cv.z_noise_level>0):
#			self.myRandsX = np.zeros((self.number_steps, self.architecture.n_x))
#			self.myRandsZ = np.zeros((self.number_steps, self.architecture.n_z))
#			def dorands(self):
#				return self.myRandsX[step_number,:],self.myRandsZ[step_number,:]




	def resetState(self):
		self.ge_x -= self.ge_x - self.leak_potential
		self.ge_z -= self.ge_z - self.leak_potential
		self.ge_m -= self.ge_m - self.leak_potential
		self.ge_mx -= self.ge_mx - self.leak_potential

		self.neurons_fired_x = []
		self.neurons_fired_z = []
		self.neurons_fired_m = []
		self.neurons_fired_mx = []
		
		self.adap_x *= 0
		self.adap_z *= 0
		
		self.znz = []
		self.xnz = []

		self.spike_neurons_x *= 0
		self.spike_neurons_z *= 0
		self.spike_neurons_m *= 0
		self.spike_neurons_mx *= 0

		
		self.open_probabilities_x *= 0
		self.open_probabilities_z *= 0
		self.open_probabilities_m *= 0
		self.open_probabilities_mx *= 0
		self.open_probabilities_x_inh *= 0
		self.open_probabilities_z_inh *= 0

		self.last_spikes_x = [[] for _dummy in xrange(int(self.cv.delay/self.cv.step_size))]
		self.last_spikes_z = [[] for _dummy in xrange(int(self.cv.delay/self.cv.step_size))]
		self.last_spikes_m = [[] for _dummy in xrange(int(self.cv.delay/self.cv.step_size))]
		self.last_spikes_mx = [[] for _dummy in xrange(int(self.cv.delay/self.cv.step_size))]


		self.x2cacheIsCurrent=False
		self.z1cacheIsCurrent=False
		#if self.cv.x_noise_level > 0:
		#	self.myRandsX *= 0
		#	self.myRandsX += np.random.normal(size=(self.number_steps, self.architecture.n_x),
		#									  scale=self.cv.x_noise_level, loc=self.cv.x_noise_loc)
		#if self.cv.z_noise_level > 0:
		#	self.myRandsZ *= 0
		#	self.myRandsZ += np.random.normal(size=(self.number_steps, self.architecture.n_z),
		#									  scale=self.cv.z_noise_level, loc=self.cv.z_noise_loc)

	def randomize_a_potential(self,ge):

		return np.random.normal(loc=mean((self.spike_threshold,self.leak_potential)),scale=abs(self.spike_threshold-self.leak_potential)/5,size=np.shape(ge)).clip(-100,self.spike_threshold-0.1)

	def randomize_potentials(self):
		self.ge_x = self.randomize_a_potential(self.ge_x)
		
		self.ge_z = self.randomize_a_potential(self.ge_z)
		self.ge_m = self.randomize_a_potential(self.ge_m)
		self.ge_mx = self.randomize_a_potential(self.ge_mx)



	def doPresentation(self, input_rates, weights, use_feedback=True):
		thesum = 0
		self.weight_update_number = weights.number_of_weight_updates

	# re-initialize the network
		self.resetState()
		self.randomize_potentials()

		# Now set up the data we are using for the problem.
 		input_rates_save = input_rates.copy()  
		input_rates = self.cv.step_size * \
			input_rates * self.cv.input_rate_scale

		self.saved_gezs=[]
		for step_number in xrange(0, self.number_steps):
			randx,randz = self.dorands()
			if self.startInput <= step_number <= self.stopInput:

				self.open_probabilities_x+=np.random.poisson(input_rates)*self.current_multiplier
				# print thesum
			if step_number == self.stopInput:
				if self.cv.reset_membrane_potential:
					self.ge_x -= self.ge_x - self.reset_potential



			#if max(self.open_probabilities_x_inh)>0:
			#	set_trace()

			self.ge_x -= (self.open_probabilities_x/self.tau_decay*(self.ge_x-self.excitatory_reversal_potential)).clip(-100,0)
			self.ge_x -= (self.open_probabilities_x_inh/self.tau_decay*(self.ge_x-self.inhibitory_reversal_potential)).clip(0,100)
			self.ge_x -= (self.ge_x - self.leak_potential-self.rmie)/self.tau_decay +(self.ge_x-self.cv.adap_voltage)*self.adap_x 

				
#			self.ge_x *=  self.time_decay
#			self.ge_x = self.ge_x.clip(self.cv.lower_bound, 21)

			self.adap_x *= self.adap_time_decay


			self.ge_z -= (self.open_probabilities_z/self.tau_decay*(self.ge_z-self.excitatory_reversal_potential)).clip(-100,0)
			self.ge_z -= (self.open_probabilities_z_inh/self.tau_decay*(self.ge_z-self.inhibitory_reversal_potential)).clip(0,100)
			self.ge_z -= (self.ge_z - self.leak_potential-self.rmie)/self.tau_decay +(self.ge_z-self.cv.adap_voltage)*self.adap_z 
				

#			self.ge_z += self.open_probabilities_z - (self.ge_z-self.cv.adap_voltage)*self.adap_z+randz 
#			self.ge_z *= self.time_decay

	

			self.ge_m -= self.open_probabilities_m/self.tau_decay*(self.ge_m-self.excitatory_reversal_potential)
			self.ge_m -= (self.ge_m- self.leak_potential-self.rmie)/self.tau_decay  

			#self.ge_m *= self.time_decay

			self.ge_mx -= self.open_probabilities_mx/self.tau_decay*(self.ge_mx-self.excitatory_reversal_potential)
			self.ge_mx -= (self.ge_mx- self.leak_potential-self.rmie)/self.tau_decay  
			#self.ge_mx *= self.time_decay



			self.adap_z *= self.adap_time_decay

			self.neurons_fired_x = self.ge_x >= self.spike_threshold
			self.neurons_fired_z = self.ge_z >= self.spike_threshold
			self.neurons_fired_m = self.ge_m >= self.spike_threshold
			self.neurons_fired_mx = self.ge_mx >= self.spike_threshold


			self.xnz = self.neurons_fired_x.nonzero()[0]
			self.znz = self.neurons_fired_z.nonzero()[0]
			self.mnz = self.neurons_fired_m.nonzero()[0]
			self.mxnz = self.neurons_fired_mx.nonzero()[0]

			self.last_spikes_x.append(self.xnz)
			self.last_spikes_z.append(self.znz)
			self.last_spikes_m.append(self.mnz)
			self.last_spikes_mx.append(self.mxnz)

			xspikes =self.last_spikes_x.pop(0)
			zspikes = self.last_spikes_z.pop(0)
			mspikes = self.last_spikes_m.pop(0)
			mxspikes = self.last_spikes_mx.pop(0)


			self.open_probabilities_z+=weights.Weff.T[:,xspikes].sum(axis=1)*self.current_multiplier
			self.open_probabilities_z_inh += weights.Winh_to_ex[:,mspikes].sum(axis=1)*self.current_multiplier
		
			self.open_probabilities_m += weights.Wex_to_inh[:,zspikes].sum(axis=1)*self.current_multiplier
			self.open_probabilities_m += weights.Wff_to_inh[:,xspikes].sum(axis=1)*self.current_multiplier

			#self.open_probabilities_m+=len(zspikes)*self.current_multiplier*self.cv.weight_to_inhib
			#self.open_probabilities_z-=len(mspikes)*self.current_multiplier*self.cv.weight_from_inhib


			if self.cv.x_noise_level:
				self.open_probabilities_x+=np.random.poisson(size=self.architecture.n_x,
											  lam=self.cv.x_noise_level*self.cv.step_size)

			if self.cv.z_noise_level:
				self.open_probabilities_z+=np.random.poisson(size=self.architecture.n_z,
											  lam=self.cv.z_noise_level*self.cv.step_size)
			if self.cv.m_noise_level:
				self.open_probabilities_m+=np.random.poisson(size=self.architecture.n_m,
											  lam=self.cv.m_noise_level*self.cv.step_size)

			if self.cv.mx_noise_level:
				self.open_probabilities_mx+=np.random.poisson(size=self.architecture.n_m,
											  lam=self.cv.mx_noise_level*self.cv.step_size)


			#self.open_probabilities_z[zpikes]+=self.current_multiplier*self.cv.lateral_inhibition
			if use_feedback:
				self.open_probabilities_x+=weights.Weffnonscale[:,zspikes].sum(axis=1)*self.current_multiplier* self.cv.feedback_fraction

			self.open_probabilities_x_inh += weights.Winh_to_ex_x[:,mxspikes].sum(axis=1)*self.current_multiplier

			self.open_probabilities_mx += weights.Wx_to_inh_x[:,xspikes].sum(axis=1)*self.current_multiplier
			self.open_probabilities_mx += weights.Wfb_to_inh[:,zspikes].sum(axis=1)*self.current_multiplier

			self.open_probabilities_z*=self.current_decay
			self.open_probabilities_x*=self.current_decay
			self.open_probabilities_m*=self.current_decay
			self.open_probabilities_mx*=self.current_decay
			self.open_probabilities_x_inh*=self.current_decay
			self.open_probabilities_z_inh*=self.current_decay

			if self.cv.adap_rise_amount_x:
				self.adap_x += self.adap_x_multiplier * \
					self.neurons_fired_x
			if len(self.znz)>0:
				self.adap_z += self.adap_z_multiplier * \
					self.neurons_fired_z

			self.ge_x[self.xnz] = self.cv.reset_potential
			self.ge_z[self.znz] = self.cv.reset_potential
			self.ge_m[self.mnz] = self.cv.reset_potential
			self.ge_mx[self.mxnz] = self.cv.reset_potential

			
			self.spike_neurons_x[step_number, :] = self.neurons_fired_x
			self.spike_neurons_z[step_number, :] = self.neurons_fired_z
			self.spike_neurons_m[step_number, :] = self.neurons_fired_m
			self.spike_neurons_mx[step_number, :] = self.neurons_fired_mx
			
			if self.cv.reset_currents == 1:
				self.open_probabilities_x[self.xnz] *=0 			
				self.open_probabilities_z[self.znz] *= 0	
				self.open_probabilities_m[self.mnz]*= 0
				self.open_probabilities_mx[self.mxnz]*= 0
				self.open_probabilities_x_inh[self.xnz] *=0 			
				self.open_probabilities_z_inh[self.znz] *= 0	


		self.input_rates = input_rates_save
		self.updateAvgFiringRates()
		self.presentation_number += 1
#		set_trace()


	def rasterPlotOld(self):
		
		import matplotlib.pyplot as plt

		#fig, ax1 = plt.subplots()
		max_yticks = 4
		yloc = plt.MaxNLocator(max_yticks)
		

		ax1=plt.subplot(2,1,2)
		scatter(self.spike_neurons_x.nonzero()[
				0]*self.cv.step_size, self.spike_neurons_x.nonzero()[1] , s=1)
		ax1.set_xlim(0,self.cvi.run_time)
		ax1.yaxis.set_major_locator(yloc)
		ax1.set_yticks(())
		ax1.hlines(0.5,0,self.cv.stimulus_on_duration,colors='gray',linewidth=8)
		ax1.text(0.8,-self.architecture.n_x/3.5,('Stimulus on'),size=10)

		xlabel(r'Time (ms)')
		#ax1.set_ylim(0,self.architecture.n_x+self.architecture.n_z)
		plt.ylabel('%d Visible neurons'%self.architecture.n_x)
		ax2 = plt.subplot(2,1,1)
		scatter(self.spike_neurons_z[:,0:min(400,self.architecture.n_z)].nonzero()[
				0]*self.cv.step_size, self.spike_neurons_z[:,0:min(400,self.architecture.n_z)].nonzero()[1], s=1, color='k')
		#ax2.set_ylim(-self.architecture.n_x,self.architecture.n_z)
		ax2.set_xlim(0,self.cvi.run_time)
		ax2.set_xticks(())
		ax2.set_yticks(())
#		ax2.spines['bottom'].set_color('gray')
#		ax1.spines['top'].set_color('gray')

		plt.ylabel('%d of %d \n Hidden neurons'%(min(400,self.architecture.n_z),self.architecture.n_z))
		#ax2.yaxis.set_major_locator(yloc)

		#gca().yaxis.set_major_formatter(formatter)
		a=plt.gcf()
		a.subplots_adjust(hspace=0, wspace=0)

	def rasterPlot(self,verbose=False,max_x = None):
		
		import matplotlib.pyplot as plt

		if max_x == None:
			max_x = self.cvi.run_time

		#fig, ax1 = plt.subplots()
		max_yticks = 4
		yloc = plt.MaxNLocator(max_yticks)
		

		ax1=plt.subplot(4,1,4)
		scatter(self.spike_neurons_x.nonzero()[
				0]*self.cv.step_size, self.spike_neurons_x.nonzero()[1] , s=1)
		ax1.set_xlim(0,max_x)
		ax1.yaxis.set_major_locator(yloc)
		ax1.set_yticks(())
		ax1.hlines(0.5,self.startInput*self.cv.step_size,self.stopInput*self.cv.step_size,colors='gray',linewidth=8)
		#ax1.text(0.8,-self.architecture.n_x/6.5,('Stimulus on'),size=10)

		xlabel(r'Time (ms)')
		if verbose:
			plt.ylabel('%d Visible neurons'%self.architecture.n_x)
		else:
			plt.ylabel('Visible\nExcit.')
		ax2 = plt.subplot(4,1,3)
		scatter(self.spike_neurons_z[:,0:min(5000,self.architecture.n_z)].nonzero()[
				0]*self.cv.step_size, self.spike_neurons_z[:,0:min(5000,self.architecture.n_z)].nonzero()[1], s=1, color='k')
		ax2.set_xlim(0,max_x)
		ax2.set_xticks(())
		ax2.set_yticks(())		
		if verbose:
			plt.ylabel('%d of %d \n Hidden neurons'%(min(400,self.architecture.n_z),self.architecture.n_z))
		else:
			plt.ylabel('Hidden\nExcit.')

		ax3 = plt.subplot(4,1,1)
		scatter(self.spike_neurons_m[:,0:min(400,self.architecture.n_m)].nonzero()[
				0]*self.cv.step_size, self.spike_neurons_m[:,0:min(400,self.architecture.n_m)].nonzero()[1], s=1, color='gray')
		ax3.set_xlim(0,max_x)
		ax3.set_xticks(())
		ax3.set_yticks(())

		if verbose:
			plt.ylabel('%d of %d \n xInhibitory neurons'%(min(400,self.architecture.n_m),self.architecture.n_m))
		else:
			plt.ylabel('Hidden\nInhib.')
		ax4 = plt.subplot(4,1,2)
		scatter(self.spike_neurons_mx[:,0:min(400,self.architecture.n_mx)].nonzero()[
				0]*self.cv.step_size, self.spike_neurons_mx[:,0:min(400,self.architecture.n_mx)].nonzero()[1], s=1, color='gray')
		ax4.set_xlim(0,max_x)
		ax4.set_xticks(())
		ax4.set_yticks(())

		if verbose:
			plt.ylabel('%d of %d \n Inhibitory neurons x'%(min(400,self.architecture.n_m),self.architecture.n_m))
		else:
			plt.ylabel('Visible\nInhib.')

		a=plt.gcf()
		a.subplots_adjust(hspace=0, wspace=0)



	def numCorrs(self,weights):
		dur=self.cv.stimulus_on_duration/self.cv.step_size
		z1num=dot(np.sum(self.spike_neurons_x[0:dur,:], axis=0),weights.W+weights.theta).clip(0,1000)
		z1=np.sum(self.spike_neurons_z[0:2.5*dur,:], axis=0)
		
		x2num=dot(z1num,weights.W.T).clip(0,100000)
		x2sum=np.sum(self.spike_neurons_x[2*dur:4*dur,:], axis=0)
		return corrcoef(z1num,z1)[0,1],corrcoef(x2num,x2sum)[0,1]





class Model(genericTwoLayer.Model):
	from models import rectified

	name = 'Integrate and fire autoencoder model'

	plasticity = integrateAndFirePlasticity

	presentation = integrateAndFirePresentation

	numericalPresentation = rectified.RectifiedPresentation

	paramstring = '''

[simulation]
runtype = iandf
learningIncreaseTime=-1
learningIncreaseValue=0
learningThetaIncreaseValue=0

[architecture]
n_z = 120

[presentation]

weight_from_inhib = 1
weight_to_inhib = 10

delay = 17 ; delay before the beginning of an EPSP, in ms
lateral_inhibition = 0

epsp_width = 30 ; width of EPSP (currently it's  a square function)
ipsp_width = 30  ; width of IPSP (currently it's  a square function)

const_excit=0

reset_currents = 1
reset_membrane_potential=0


reset_potential=-70
spike_threshold = -54
leak_potential=-70
rmgs = 0.05
excitatory_reversal_potential = 0
inhibitory_reversal_potential = -80
rmie = 0
# all taken from dyan and abbot p.188, except set rmie to 0


stimulus_on_duration=20 ; duration of the stimulus presentaiton, in ms (starts at time=0)
stimulus_start_time = 0

input_rate_scale = 15 ;  amount by which to multiply input pixel values 
#(after normalizing by std?)

tau_decay = 30 ; time for decay of membrane potential, in ms
tau_adap_decay = 30 ; time for decay of  spike rate adaptation, in ms
tau_current_decay = 2.0
adap_rise_amount_x = 0.0 ; rise amount in adaptation variable per ms
adap_rise_amount_z=0 ; rise amount in adaptation variable per ms


feedback_fraction= 0.25 ; the amount by which FB connections are scaled 
# relative to FF ones
# excitation for the same time difference betwen spikes
inh_level=0


x_noise_level=.0
z_noise_level=0
x_noise_loc=-0
z_noise_loc=0

m_noise_level=0
m_noise_loc=0

mx_noise_level=0
mx_noise_loc=0


use_spike_count_for_theta = 0

x_bias=0.0
bias_learning_rate_factor=1



lower_bound = -40
adap_voltage=-80

run_time=100 ; presentation running time, in ms
step_size=0.1 ; step size, in ms

use_standard_bias=0

[plasticity]
weight_regularization=0
tau_LTP = 30 ; time constant for STDP, in ms
tau_LTD = 30 ; time constant for STDP, in ms
max_dw = 30000000
weight_change_probability=1
depression_bias = 4 ; the amount by which depression is stronger than 
learningRate=.01
thetaLearningRate=0.1
max_firing_rate=1

[weights]
use_symmetric_weights=1
initial_theta_mean=-0.01
initial_weight_scale=0.25

		'''
class BurstExclusionModel(Model):
	from models import rectified
	weights = rectified.MultiplicativeWeights
	plasticity = PlasticityExcludingBursts

	paramstring = '''
[plasticity]
exclusion_threshold=200
[weights]
min_theta=0
max_theta=100

'''

class MultiplicativeScalingModel(Model):
	from models import rectified
	weights = rectified.MultiplicativeWeights
	paramstring = '''
[weights]
min_theta=0
max_theta=100
'''

class MultiplicativeInhibitionModel(MultiplicativeScalingModel):
	from models import rectified
	weights = rectified.MultiplicativeInhibitiveWeights
	paramstring = '''
[weights]
weight_from_inhib=0.01
weight_to_inhib=.1
weight_x_to_inhib = 0.1
weight_x_from_inhib = 0.1
weight_ff_to_inhib = 0.1
weight_fb_to_inhib = 0.1

[architecture]
n_m=1000
n_mx = 300
'''



class InhibitionModel(Model):
	weights = genericTwoLayer.WeightsPlussInhib

	paramstring = '''
[weights]
weight_from_inhib=0.01
weight_to_inhib=.1
weight_x_to_inhib = 0.1
weight_x_from_inhib = 0.1
weight_ff_to_inhib = 0.1
weight_fb_to_inhib = 0.1

[architecture]
n_m=1000
n_mx = 300
'''


class NonSymmetricWeightsModel(Model):

	name = 'Integrate and fire, non symmetric initial weights'

	weights = genericTwoLayer.NonSymmetricWeights
